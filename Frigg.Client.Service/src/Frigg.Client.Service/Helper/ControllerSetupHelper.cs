﻿using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Frigg.Client.Service.Helper
{
    public static class ControllerSetupHelper
    {
        private static readonly TelemetryClient telemetryClient = Helper.GetTelemetryClient();
        private static readonly int PrivateChargerValue = 821200001;
        private static readonly Dictionary<int, int> ChargerLValues =
            new Dictionary<int, int>()
            {
                {1, 821200000},
                {2, 821200001},
                {3, 821200002}
            };

        public static Guid CreateController(CrmServiceClient service, ControllerSetupRequestMessage request)
        {
            var entity = new Entity("gk_controller");

            entity["gk_name"] = request.ControllerName;
            entity["gk_customerid"] = new EntityReference("account", new Guid(request.DynamicsCustomerId));
            entity["gk_locationid"] = new EntityReference("gk_location", new Guid(request.DynamicsLocationId));
            entity["gk_serialnumber"] = request.SerialNumber;
            entity["gk_uuid"] = Guid.NewGuid().ToString();

            if (request.FuseSize.HasValue)
            {
                entity["gk_fusesize"] = request.FuseSize;
            }
            if (request.Headroom.HasValue)
            {
                entity["gk_headroom"] = request.Headroom.Value;
            }
            if (request.Ampere.HasValue)
            {
                entity["gk_numberofampereonct"] = request.Ampere.Value;
            }
            if (request.SensorType.HasValue)
            {
                entity["gk_sensortype"] = new OptionSetValue(request.SensorType.Value);
            }
            if (request.CurrentSensorType.HasValue)
            {
                entity["gk_currentsensortype"] = new OptionSetValue(request.CurrentSensorType.Value);
            }

            var controllerId = service.Create(entity);

            return controllerId;
        }

        public static void AttachControllerInAcProject(CrmServiceClient service, ControllerSetupRequestMessage request, Guid controllerId)
        {
            var entity = new Entity("gk_acproject")
            {
                Id = new Guid(request.AcProjectId)
            };

            entity["gk_sizeofmainfuse" + request.ControllerIndex] = request.FuseSize;
            entity["gk_controller" + request.ControllerIndex + "id"] = new EntityReference("gk_controller", controllerId);

            service.Update(entity);
        }

        public static List<Clusters> CreateClusters(CrmServiceClient service, ControllerSetupRequestMessage request, Guid controllerId, TransactionLogger logger)
        {
            var listOfClusters = new List<Clusters>();

            if (request.ClusterDetails != null && request.ClusterDetails.Count > 0)
            {
                foreach (var data in request.ClusterDetails)
                {
                    var entity = new Entity("gk_cluster");

                    entity["gk_name"] = RemoveWhiteSpaces(data.Name);
                    entity["gk_controllerid"] = new EntityReference("gk_controller", controllerId);

                    if (data.Limit.HasValue)
                    {
                        entity["gk_limit"] = data.Limit.Value;
                    }
                    if (data.Headroom.HasValue)
                    {
                        entity["gk_headroom"] = data.Headroom.Value;
                    }
                    if (data.Priority.HasValue)
                    {
                        entity["gk_priority"] = data.Priority;
                    }

                    entity["gk_nettype"] = new OptionSetValue(request.NetType);

                    var id = service.Create(entity);

                    if (id != null)
                    {
                        logger.StatusMessage(1, "<div>" + GetLogValues(10, data.Name + " created.") + "</div>");

                        var clusters = new Clusters
                        {
                            ClusterId = id,
                            Name = data.Name,
                            ParentCluster = data.ParentCluster
                        };

                        listOfClusters.Add(clusters);
                    }
                    else
                    {
                        telemetryClient.TrackTrace($"Cluster {data.Name} of Controller {request.ControllerName} is not created as error has been occurred.");
                        logger.StatusMessage(1, "<div class='error'>" + GetLogValues(10, data.Name + " is not created as error has been occurred.") + "</div>");
                    }
                }
            }
            else
            {
                CreateRootCluster(service, request, controllerId, logger);
            }


            return listOfClusters;
        }

        private static void CreateRootCluster(CrmServiceClient service, ControllerSetupRequestMessage request, Guid controllerId, TransactionLogger logger)
        {
            var entity = new Entity("gk_cluster");

            entity["gk_name"] = "root";
            entity["gk_controllerid"] = new EntityReference("gk_controller", controllerId);

            if (request.Headroom.HasValue)
            {
                entity["gk_headroom"] = request.Headroom.Value;
            }

            entity["gk_nettype"] = new OptionSetValue(request.NetType);

            var id = service.Create(entity);

            if (id != null)
            {
                logger.StatusMessage(1, "<div>" + GetLogValues(10, " root created.") + "</div>");
            }
            else
            {
                telemetryClient.TrackTrace($"Cluster root of Controller {request.ControllerName} is not created as error has been occurred.");
                logger.StatusMessage(1, "<div class='error'>" + GetLogValues(10, "root is not created as error has been occurred.") + "</div>");
            }
        }

        public static void SetParentClusters(CrmServiceClient service, List<Clusters> clusterList)
        {
            foreach (var data in clusterList)
            {
                var entity = new Entity("gk_cluster")
                {
                    Id = data.ClusterId
                };

                if (!string.IsNullOrEmpty(data.ParentCluster))
                {
                    var cluster = clusterList.Where(x => x.Name == data.ParentCluster)?.FirstOrDefault();

                    if (cluster != null)
                    {
                        entity["gk_parentclusterid"] = new EntityReference("gk_cluster", cluster.ClusterId);

                        service.Update(entity);
                    }
                }
            }
        }

        public static string GetLogValues(int spaceLength, string log)
        {
            var space = string.Empty;

            for (var i = 0; i <= spaceLength; i++)
            {
                space += "&#160;";
            }

            return space + log;
        }

        public static void CreateChargers(CrmServiceClient service, ControllerSetupRequestMessage request, TransactionLogger logger)
        {
            if (request.NumberOfChargers > 0)
            {
                var connectorCount = 0;

                var connectorDetails = GenerateConnectorDetails(service, request);

                for (var i = 0; i < request.NumberOfChargers; i++)
                {
                    var entity = new Entity("gk_charger");

                    entity["gk_name"] = "Unknown";
                    entity["gk_chargerproductid"] = new EntityReference("gk_product", new Guid(request.DynamicsProductId));
                    entity["gk_location"] = new EntityReference("gk_location", new Guid(request.DynamicsLocationId));
                    entity["gk_chargepointname"] = "Unknown";
                    entity["gk_endpointtype"] = "GraphQL";
                    entity["gk_availability"] = new OptionSetValue(PrivateChargerValue);

                    var values = GetChargerLValues(i);
                    var netType = (NetType)Enum.Parse(typeof(NetType), request.NetType.ToString());
                    var phaseMapping = string.Empty;

                    if (NetType.TN == netType)
                    {
                        phaseMapping = "N" + values[0] + values[1] + values[2];
                        entity["gk_l1"] = new OptionSetValue(ChargerLValues[values[0]]);
                        entity["gk_l2"] = new OptionSetValue(ChargerLValues[values[1]]);
                        entity["gk_l3"] = new OptionSetValue(ChargerLValues[values[2]]);
                    }
                    else if (NetType.IT == netType)
                    {
                        phaseMapping = SetUpPhaseValue(values[0], values[1]);
                        entity["gk_n"] = new OptionSetValue(ChargerLValues[values[0]]);
                        entity["gk_l1"] = new OptionSetValue(ChargerLValues[values[1]]);
                    }
                    var chargerId = service.Create(entity);

                    if (chargerId != null)
                    {
                        connectorCount += CreateConnectors(service, connectorDetails, request.DynamicsLocationId, chargerId, phaseMapping);
                    }
                }

                logger.StatusMessage(1, "<div>" + GetLogValues(0, "Total " + request.NumberOfChargers + " chargers and total " + connectorCount + " connectors are created.") + "</div>");
            }
        }

        public static List<ConnectorDetails> GenerateConnectorDetails(CrmServiceClient service, ControllerSetupRequestMessage request)
        {
            var details = new List<ConnectorDetails>();

            var columns = new string[] { "gk_startchargingscenario", "gk_connector1id", "gk_connector2id", "gk_connector3id", "gk_connector4id" };
            var entity = CRMHelper.GetEntity(service, "gk_product", "gk_productid", request.DynamicsProductId.ToString(), columns);

            if (entity != null)
            {
                int? chargingScenario = null;

                if (entity.Attributes.Contains("gk_startchargingscenario"))
                {
                    chargingScenario = CRMHelper.GetOptionSetValue(entity.Attributes, "gk_startchargingscenario");
                }
                if (entity.Attributes.Contains("gk_connector1id"))
                {
                    var model = new ConnectorDetails();
                    model.ConnectorType = CRMHelper.GetLookupIdValue(entity.Attributes, "gk_connector1id");
                    model.ConnectorLevel = 1;
                    model.StartChargringScenario = chargingScenario;

                    details.Add(model);
                }
                if (entity.Attributes.Contains("gk_connector2id"))
                {
                    var model = new ConnectorDetails();
                    model.ConnectorType = CRMHelper.GetLookupIdValue(entity.Attributes, "gk_connector2id");
                    model.ConnectorLevel = 2;
                    model.StartChargringScenario = chargingScenario;

                    details.Add(model);
                }
                if (entity.Attributes.Contains("gk_connector3id"))
                {
                    var model = new ConnectorDetails();
                    model.ConnectorType = CRMHelper.GetLookupIdValue(entity.Attributes, "gk_connector3id");
                    model.ConnectorLevel = 3;
                    model.StartChargringScenario = chargingScenario;

                    details.Add(model);
                }
                if (entity.Attributes.Contains("gk_connector4id"))
                {
                    var model = new ConnectorDetails();
                    model.ConnectorType = CRMHelper.GetLookupIdValue(entity.Attributes, "gk_connector4id");
                    model.ConnectorLevel = 4;
                    model.StartChargringScenario = chargingScenario;

                    details.Add(model);
                }
            }

            return details;
        }

        public static int CreateConnectors(CrmServiceClient service, List<ConnectorDetails> connectorDetails, string dynamicsLocationId, Guid chargerId, string phaseMapping)
        {
            var count = 0;

            if (connectorDetails.Count > 0)
            {
                foreach (var data in connectorDetails)
                {
                    var entity = new Entity("gk_connector");

                    entity["gk_name"] = "Unknown";

                    if (data.ConnectorType.HasValue)
                    {
                        entity["gk_connectortypeid"] = new EntityReference("gk_connectortype", data.ConnectorType.Value);
                    }
                    if (data.ConnectorLevel.HasValue)
                    {
                        entity["gk_connectorlevel"] = new OptionSetValue(data.ConnectorLevel.Value);
                    }
                    if (data.StartChargringScenario.HasValue)
                    {
                        entity["gk_startchargingscenario"] = new OptionSetValue(data.StartChargringScenario.Value);
                    }
                    if (!string.IsNullOrEmpty(phaseMapping))
                    {
                        entity["gk_phasemapping"] = phaseMapping;
                    }
                    entity["gk_smscode"] = "Unknown";
                    entity["gk_chargerid"] = new EntityReference("gk_charger", chargerId);
                    entity["gk_locationid"] = new EntityReference("gk_location", new Guid(dynamicsLocationId));
                    entity["gk_connectorstatus"] = new OptionSetValue(821200004);

                    var connectorId = service.Create(entity);

                    if (connectorId != null)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        public static int[] GetChargerLValues(int chargerNumber)
        {
            int[] array = new int[] { 1, 2, 3 };

            var length = array.Length;

            for (var i = 0; i < chargerNumber; i++)
            {
                var temp = array[0];
                for (var j = 0; j < length - 1; j++)
                {
                    array[j] = array[j + 1];
                }

                array[length - 1] = temp;
            }

            return array;
        }

        public static string RemoveWhiteSpaces(string str)
        {
            return Regex.Replace(str, @"\s+", String.Empty);
        }

        public static string SetUpPhaseValue(int value1, int value2)
        {
            try
            {
                string phaseValue = "xxxx";
                StringBuilder sb = new StringBuilder(phaseValue);
                sb[value1] = 'N';
                sb[value2] = 'L';
                phaseValue = sb.ToString();
                return phaseValue;
            }
            catch (Exception ex)
            {
                telemetryClient.TrackTrace("Error while creating phase value for IT type.");
                telemetryClient.TrackException(ex);
                return string.Empty;
            }
        }
    }
}