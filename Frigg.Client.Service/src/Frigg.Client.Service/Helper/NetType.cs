﻿namespace Frigg.Client.Service.Helper
{
    public enum NetType
    {
        IT = 821200000,
        TT = 821200001,
        TN = 821200002
    }
}