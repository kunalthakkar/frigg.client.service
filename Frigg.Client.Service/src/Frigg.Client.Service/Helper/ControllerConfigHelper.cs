﻿using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Helper
{
    public class ControllerConfigHelper
    {
        private readonly TransactionLogger _logger;
        private readonly TelemetryClient _telemetryClient;

        public ControllerConfigHelper(TransactionLogger logger, TelemetryClient telemetryClient)
        {
            _logger = logger;
            _telemetryClient = telemetryClient;
        }


        public void ControllerConfigRequest(ControllerConfigRequestMessage request)
        {

            var connectionString = AzureHelper.GetValueFromAzureVault("DynamicsConnectionWithAppUser");
            var service = CRMHelper.GetCRMService(connectionString + ";RequireNewInstance=true");
            Guid controllerId = new Guid(request.ControllerId);
            Entity controller = service.Retrieve("gk_controller", controllerId, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
            if (controller != null)
            {
                if (controller.Attributes.Contains("gk_serialnumber"))
                {
                    string serialNumber = Convert.ToString(controller.Attributes["gk_serialnumber"]);

                    GraphQLHelper graphQLHelper = new GraphQLHelper(_logger, _telemetryClient);
                    string token = graphQLHelper.CreateControllerV2NewToken();

                    switch (request.ControllerConfigRequestType)
                    {
                        case ControllerConfigRequestEnum.ScanController:
                            graphQLHelper.CallScanControllerGraphQL(serialNumber,service,controller,token,request.VendorId);
                            break;
                        case ControllerConfigRequestEnum.InitiateChargepointController:
                            graphQLHelper.CallInitiateChargePointGraphQL(serialNumber,controller,service, token, request.VendorId,request.ChargerId,request.IsSelectAll);
                            break;
                        case ControllerConfigRequestEnum.ConfigureController:
                            graphQLHelper.CallConfigureControllerGraphQL(serialNumber,controller,service, token);
                            break;
                        case ControllerConfigRequestEnum.SetSensorAnalogController:
                            graphQLHelper.CallSetSensorAnalogGraphQL(serialNumber, controller, service, token);
                            break;
                        case ControllerConfigRequestEnum.SetSensorModbusController:
                            graphQLHelper.CallSetSensorModbusGraphQL(serialNumber, token);
                            break;
                        case ControllerConfigRequestEnum.AllEvents:
                            graphQLHelper.CallScanControllerGraphQL(serialNumber,service,controller, token, request.VendorId);
                            graphQLHelper.CallInitiateChargePointGraphQL(serialNumber, controller, service, token, request.VendorId);
                            graphQLHelper.CallConfigureControllerGraphQL(serialNumber, controller, service, token);
                            if (controller.Attributes.Contains("gk_sensortype"))
                            {
                                int sensorType = CRMHelper.GetOptionSetValue(controller.Attributes, "gk_sensortype");
                                if (sensorType == (int)EnumControllerSensorType.Analog)
                                {
                                    graphQLHelper.CallSetSensorAnalogGraphQL(serialNumber, controller, service, token);
                                }
                                else if (sensorType == (int)EnumControllerSensorType.Modbus)
                                {
                                    graphQLHelper.CallSetSensorModbusGraphQL(serialNumber, token);
                                }
                            }
                            break;
                    }
                }
                else
                {
                    _logger.StatusMessage(1, "<div> Serial Number not found.</div>");
                    _telemetryClient.TrackTrace("Serial Number not found.");
                }
            }
            else
            {
                _logger.StatusMessage(1, "<div> Controller not found.</div>");
                _telemetryClient.TrackTrace("Controller not found.");
            }


        }

       

    }
}