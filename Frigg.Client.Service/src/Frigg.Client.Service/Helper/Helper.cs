﻿using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;
using System.Configuration;

namespace Frigg.Client.Service.Helper
{
    public static class Helper
    {
        public static TelemetryClient GetTelemetryClient()
        {
            var configuration = TelemetryConfiguration.CreateDefault();
            configuration.InstrumentationKey = ConfigurationManager.AppSettings["APPINSIGHTS_INSTRUMENTATIONKEY"].ToString();              
            var telemetryClient = new TelemetryClient(configuration);

            return telemetryClient;
        }
    }
}