﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using Frigg.Client.Service.Models;
using System.Configuration;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using GK.Helper;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Linq;

namespace Frigg.Client.Service.Helper
{
    public class GraphQLHelper
    {
        private readonly TransactionLogger _logger;
        private readonly TelemetryClient _telemetryClient;

        public GraphQLHelper(TransactionLogger logger, TelemetryClient telemetryClient)
        {
            _logger = logger;
            _telemetryClient = telemetryClient;
        }

        public static string GraphQLRequest(GraphQlQuery graphQl, string token)
        {
            string response = string.Empty;
            using (var client = new HttpClient())
            {
                var bodyContent = new StringContent(JsonConvert.SerializeObject(graphQl), Encoding.UTF8, "application/json");
                string grphQLURL = ConfigurationManager.AppSettings["GraphQLURL"].ToString();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var result = client.PostAsync(grphQLURL, bodyContent).Result;
                response = result.Content.ReadAsStringAsync().Result;
            }
            return response;
        }

        public void CallScanControllerGraphQL(string serialNumber, CrmServiceClient service, Entity controller, string token, string lstVendorId)
        {
            _logger.StatusMessage(1, $"<div>Serial Number : {serialNumber}</div>");
            _telemetryClient.TrackTrace($"Serial Number  {serialNumber}");

            int timeOutMs = Convert.ToInt32(ConfigurationManager.AppSettings["TimeOutMs"]);

            List<int> commaSeperatedVendorId = lstVendorId.Split(',').Select(int.Parse).ToList();
            if (commaSeperatedVendorId != null)
            {
                foreach (var vendorId in commaSeperatedVendorId)
                {
                    string vendorName = Convert.ToString((EnumVendor)vendorId);
                    _logger.StatusMessage(1, $"<br/><div>Scan Controller Request execution started for Vendor - : {vendorName}</div>");
                    _telemetryClient.TrackTrace($"Serial Number  {serialNumber}");

                    GraphQlQuery graphQl = new GraphQlQuery();
                    var query = $@"mutation{{
                            scan(input:
                            {{
                                deviceId : ""{serialNumber}"",
                                timeoutMs : {timeOutMs},
                                vendor : {vendorName}
                            }})
                            {{ id,ip,model, serialNumber,vendor }}
                         }}";

                    graphQl.Query = query;

                    _telemetryClient.TrackTrace($"Scan Controller GraphQL request for Vendor -  {vendorName} : {JsonConvert.SerializeObject(graphQl)}");

                    var result = GraphQLHelper.GraphQLRequest(graphQl, token);
                    _telemetryClient.TrackTrace($"Scan Controller GraphQL result for Vendor -  {vendorName} : {result}");

                    var resultData = JsonConvert.DeserializeObject<dynamic>(result);

                    var dynamicsChargePoints = GetChargers(service, controller.Id);
                    if (dynamicsChargePoints != null)
                    {
                        _logger.StatusMessage(1, $"<div style='margin-left:20px;'> Total {dynamicsChargePoints.Entities.Count} Charger(s) found in Frigg for Vendor -  {vendorName}.</div>");

                        if (resultData != null && resultData.data != null && Convert.ToString(resultData.data) != "{}")
                        {
                            _logger.StatusMessage(1, $"<div>Scan Controller GraphQL result for Vendor -  {vendorName} : {resultData.data}</div>");

                            ScanControllerResultModel scanControllerResultModel = JsonConvert.DeserializeObject<ScanControllerResultModel>(Convert.ToString(resultData.data));
                            if (scanControllerResultModel.Scan != null && scanControllerResultModel.Scan.Count > 0)
                            {
                                _logger.StatusMessage(1, $"<div style='margin-left:20px;'> Total {scanControllerResultModel.Scan.Count} Charger(s) found in Controller Scan request for Vendor -  {vendorName}.</div>");

                                string grid = GetChargerGrid(dynamicsChargePoints, scanControllerResultModel.Scan, service, controller.Id);
                                _logger.StatusMessage(1, $"<div style='margin-left:40px; margin-top:10px;'> { grid }</div>");
                            }
                            else
                            {
                                _logger.StatusMessage(1, "<div style='margin-top:10px;'>Controller not found in Controller Scan request for Vendor -  {vendorName}.</div>");
                            }

                            _logger.StatusMessage(1, $"<div style='margin-top:10px;'> Scan Controller GraphQL executes successfully for Vendor -  {vendorName}.</div>");
                            _telemetryClient.TrackTrace($"Scan Controller GraphQL executes successfully for Vendor -  {vendorName}.");
                        }
                        else
                        {
                            if (resultData != null && resultData.errors != null)
                            {
                                _telemetryClient.TrackTrace($"Scan Controller GraphQL Error for Vendor -  {vendorName} : {resultData.errors.ToString()}");
                                _logger.StatusMessage(1, $"<div>Scan Controller GraphQL Error for Vendor -  {vendorName} :</div>");
                                GraphQLErrorLog(Convert.ToString(resultData));
                            }

                            _logger.StatusMessage(1, $"<div> Scan Controller GraphQL fail for Vendor -  {vendorName}.</div>");
                        }
                    }
                    else
                    {
                        _logger.StatusMessage(1, $"<div style='color:red; margin-top:10px;'>Chargers not found for this controller.</div>");
                        _telemetryClient.TrackTrace("Chargers not found for this controller.");
                    }
                    _logger.StatusMessage(1, $"<div>Scan Controller Request execution completed for Vendor -  {vendorName}.</div>");
                    _telemetryClient.TrackTrace("Scan Controller Request execution completed for Vendor -  {vendorName}.");
                }
            }
            else
            {
                _logger.StatusMessage(1, "<div>VendorId not found for Scan Controller Request.</div>");
                _telemetryClient.TrackTrace("VendorId not found for Scan Controller Request.");
            }
        }

        private string GetChargerGrid(EntityCollection dynamicsChargerList, List<Scan> controllerChargeList, CrmServiceClient service, Guid controllerId)
        {
            string table = "<table border='1'><tr><td>FRIGG</td><td>SCAN CONTROLLER</td></tr>";
            foreach (var dynamicsCharger in dynamicsChargerList.Entities)
            {
                string dynamicsSerialNumber = string.Empty;
                if (dynamicsCharger.Attributes.Contains("gk_serialnumber"))
                {
                    dynamicsSerialNumber = Convert.ToString(dynamicsCharger["gk_serialnumber"]);
                    if (controllerChargeList.Exists(x => x.SerialNumber == dynamicsSerialNumber))
                    {
                        table += $@"<tr><td style='color:Green;'> {dynamicsSerialNumber}</td>
                                        <td style='color:Green;'>{dynamicsSerialNumber}</td>
                                    </tr>";
                    }
                    else
                    {
                        table += $@"<tr><td style='color:red;'> {dynamicsSerialNumber}</td>
                                        <td></td>
                                    </tr>";
                    }
                }
            }
            foreach (var controllerCharger in controllerChargeList)
            {
                Entity charger = GetActiveChargerBySerialId(service, controllerCharger.SerialNumber, controllerId);
                if (charger != null)
                {
                    Entity chargerUpdate = new Entity(charger.LogicalName, charger.Id);
                    chargerUpdate["gk_ip"] = controllerCharger.IP;
                    service.Update(chargerUpdate);
                }
                else
                {
                    table += $@"<tr><td></td>
                                    <td style='color:red;'> {controllerCharger.SerialNumber}</td>
                                    </tr>";
                }
            }
            table += "</table>";
            return table;
        }

        public void CallInitiateChargePointGraphQL(string serialNumber, Entity controller, CrmServiceClient service, string token, string vendorIds, string chargerIds = "", bool isSelectAll = true)
        {
            _logger.StatusMessage(1, $"<div style='margin-top:10px;'>InitiateChargepoint Controller Request execution started.</div>");

            _logger.StatusMessage(1, $"<div>Serial Number : {serialNumber}</div>");
            _telemetryClient.TrackTrace($"Serial Number  {serialNumber}");

            var listOfVendorId = vendorIds.Split(',').Select(int.Parse).ToList();

            List<string> listOfChargerId = null;
            if (isSelectAll == false && !string.IsNullOrEmpty(chargerIds))
            {
                listOfChargerId = chargerIds.Split(',').ToList();
            }

            if (listOfVendorId?.Count > 0)
            {
                var chargePointDetails = GetChargePointDetails(service, controller.Id, listOfChargerId);
                var chargePoints = GetChargePointForInitiateController(chargePointDetails, listOfVendorId);

                GraphQlQuery graphQl = new GraphQlQuery();
                var query = $@"mutation{{
                            initiateChargepoints(input:
                            {{
                                deviceId : ""{serialNumber}"",
                                setup : 
                                [
                                {chargePoints}
                                ]
                            }})
                         }}";

                graphQl.Query = query;
                _telemetryClient.TrackTrace($"InitiateChargepoints GraphQL request : {JsonConvert.SerializeObject(graphQl)}");

                var result = GraphQLRequest(graphQl, token);

                _telemetryClient.TrackTrace($"InitiateChargePoints  GraphQL result : {result}");

                var resultData = JsonConvert.DeserializeObject<dynamic>(result);

                if (resultData != null && resultData.data != null && Convert.ToString(resultData.data) != "{}")
                {
                    _logger.StatusMessage(1, $"<div> InitiateChargePoints GraphQL result : {resultData.data}.</div>");

                    UpdateChargePointInitiatedField(service, chargePointDetails);

                    _logger.StatusMessage(1, "<div>InitiateChargePoint GraphQL executes successfully.</div>");
                    _telemetryClient.TrackTrace("InitiateChargePoint GraphQL executes successfully.");
                }
                else
                {
                    if (resultData != null && resultData.errors != null)
                    {
                        _telemetryClient.TrackTrace($"InitiateChargePoint GraphQL Error {resultData.errors.ToString()}");
                        _logger.StatusMessage(1, $"<div>InitiateChargePoint GraphQL Error :</div>");
                        GraphQLErrorLog(Convert.ToString(resultData));
                    }

                    _logger.StatusMessage(1, "<div> InitiateChargePoint GraphQL fail.</div>");
                }

                _logger.StatusMessage(1, "<div>InitiateChargepointController Request execution completed.</div>");
                _telemetryClient.TrackTrace("InitiateChargepointController Request execution completed.");
            }
            else
            {
                _logger.StatusMessage(1, "<div>VendorId not found for Initiate Controller Request.</div>");
                _telemetryClient.TrackTrace("VendorId not found for Initiate Controller Request.");
            }
        }

        public void CallConfigureControllerGraphQL(string serialNumber, Entity controller, CrmServiceClient service, string token)
        {
            _logger.StatusMessage(1, $"<div style='margin-top:10px;'>ConfigureController Request execution started.</div>");

            _logger.StatusMessage(1, $"<div>Serial Number : {serialNumber}</div>");
            _telemetryClient.TrackTrace($"Serial Number  {serialNumber}");

            int fuzeSize = 0;
            string sensorType = "root";
            if (controller.Attributes.Contains("gk_fusesize"))
            {
                fuzeSize = Convert.ToInt32(controller["gk_fusesize"]);
            }

            string chargePoints = GetChargePointsForConfigureController(controller.Id, service);
            if (!string.IsNullOrEmpty(chargePoints))
            {
                string clusters = GetClusterPoints(controller.Id, service, fuzeSize, sensorType);

                GraphQlQuery graphQl = new GraphQlQuery();
                var query = $@"mutation{{
                            configureControllerNetwork(input:
                            {{
                                    deviceId : ""{serialNumber}"",
                                    chargePoints :
                                    [
                                    {chargePoints}
                                    ],
                                    clusters :
                                    [
                                    {clusters}
                                    ]
                            }})
                         }}";

                graphQl.Query = query;

                _telemetryClient.TrackTrace($"ConfigureController  GraphQL request : {JsonConvert.SerializeObject(graphQl)}");

                var result = GraphQLHelper.GraphQLRequest(graphQl, token);

                _telemetryClient.TrackTrace($"ConfigureController  GraphQL result : {result}");

                var resultData = JsonConvert.DeserializeObject<dynamic>(result);

                if (resultData != null && resultData.data != null && Convert.ToString(resultData.data) != "{}")
                {
                    _logger.StatusMessage(1, $"<div> ConfigureController GraphQL result : {resultData.data}.</div>");
                    _logger.StatusMessage(1, "<div>ConfigureController GraphQL executes successfully.</div>");
                    _telemetryClient.TrackTrace("ConfigureController GraphQL executes successfully.");
                }
                else
                {
                    if (resultData != null && resultData.errors != null)
                    {
                        _telemetryClient.TrackTrace($"ConfigureController GraphQL Error {resultData.errors.ToString()}");
                        _logger.StatusMessage(1, $"<div>ConfigureController GraphQL Error :</div>");
                        GraphQLErrorLog(Convert.ToString(resultData));
                    }

                    _logger.StatusMessage(1, "<div> ConfigureController GraphQL fail.</div>");
                }
            }
            else
            {
                _logger.StatusMessage(1, $"<div style='color:red; margin-top:10px;'>Chargers not found for this controller.</div>");
                _telemetryClient.TrackTrace("Chargers not found for this controller.");
            }
            _logger.StatusMessage(1, "<div>ConfigureController Request execution completed.</div>");
            _telemetryClient.TrackTrace("ConfigureController Request execution completed.");
        }

        public void CallSetSensorAnalogGraphQL(string serialNumber, Entity controller, CrmServiceClient service, string token)
        {
            _logger.StatusMessage(1, $"<div style='margin-top:10px;'>SetSensorAnalogController Request execution started.</div>");

            _logger.StatusMessage(1, $"<div>Serial Number : {serialNumber}</div>");
            _telemetryClient.TrackTrace($"Serial Number  {serialNumber}");

            EnumCurrentSensorType cureentSensorType = EnumCurrentSensorType.CT;
            decimal inputVoltage = 0;
            decimal outputCurrent = 0;
            if (controller.Attributes.Contains("gk_currentsensortype"))
            {
                int sensorType = CRMHelper.GetOptionSetValue(controller.Attributes, "gk_currentsensortype");
                if (sensorType == (int)EnumCurrentSensorType.ROGOWSKI)
                {
                    cureentSensorType = EnumCurrentSensorType.ROGOWSKI;
                }
            }
            if (controller.Attributes.Contains("gk_inputvoltage"))
            {
                inputVoltage = Convert.ToDecimal(CRMHelper.GetOptionSetText(controller, "gk_inputvoltage"));
            }
            if (controller.Attributes.Contains("gk_outputcurrent"))
            {
                outputCurrent = Convert.ToDecimal(CRMHelper.GetOptionSetText(controller, "gk_outputcurrent"));
            }


            GraphQlQuery graphQl = new GraphQlQuery();
            var query = $@"mutation{{
                            setSensorAnalog(input:
                            {{
                                currentSensorType : {cureentSensorType},
                                deviceId : ""{serialNumber}"",
                                inputVoltage : {inputVoltage},
                                name : ""root"",
                                outputCurrent : {outputCurrent}
                            }})
                         }}";

            graphQl.Query = query;

            _telemetryClient.TrackTrace($"SetSensorAnalog  GraphQL request : {JsonConvert.SerializeObject(graphQl)}");

            var result = GraphQLHelper.GraphQLRequest(graphQl, token);
            _telemetryClient.TrackTrace($"SetSensorAnalog  GraphQL result : {result}");

            var resultData = JsonConvert.DeserializeObject<dynamic>(result);

            if (resultData != null && resultData.data != null && Convert.ToString(resultData.data) != "{}")
            {
                _logger.StatusMessage(1, $"<div> SetSensorAnalog GraphQL result : {resultData.data}.</div>");
                _logger.StatusMessage(1, "<div>SetSensorAnalog GraphQL executes successfully.</div>");
                _telemetryClient.TrackTrace("SetSensorAnalog GraphQL executes successfully.");
            }
            else
            {
                if (resultData != null && resultData.errors != null)
                {
                    _telemetryClient.TrackTrace($"SetSensorAnalog GraphQL Error {resultData.errors.ToString()}");
                    _logger.StatusMessage(1, $"<div>SetSensorAnalog GraphQL Error :</div>");
                    GraphQLErrorLog(Convert.ToString(resultData));
                }

                _logger.StatusMessage(1, "<div> SetSensorAnalog GraphQL fail.</div>");
            }
            _logger.StatusMessage(1, "<div>SetSensorAnalogController Request execution completed.</div>");
            _telemetryClient.TrackTrace("SetSensorAnalogController Request execution completed.");
        }


        public string GetChargePointsForConfigureController(Guid controllerId, CrmServiceClient service)
        {
            string result = string.Empty;
            string parentClusterName = "root";
            var lstChargers = GetChargers(service, controllerId);
            if (lstChargers != null && lstChargers.Entities.Count > 0)
            {
                foreach (var charger in lstChargers.Entities)
                {
                    string chargerName = string.Empty;
                    EnumAccessControlType enumAccessControlType = EnumAccessControlType.OPEN;
                    if (charger.Attributes.Contains("gk_accesscontrol"))
                    {
                        int accessControl = CRMHelper.GetOptionSetValue(charger.Attributes,"gk_accesscontrol");
                        enumAccessControlType = GetEnumAccessControlType(accessControl, enumAccessControlType);
                    }
                    if (charger.Attributes.Contains("gk_name"))
                    {
                        chargerName = Convert.ToString(charger["gk_name"]);
                    }
                    if (charger.Attributes.Contains("gk_clusterid"))
                    {
                        var cluster = (EntityReference)charger["gk_clusterid"];
                        parentClusterName = cluster.Name;
                    }

                    string connectors = GetConnectors(charger.Id, service);
                    result += $@"{{
                                        id : ""{chargerName}"",
                                        accessControl : {enumAccessControlType},
                                        cluster : ""{parentClusterName}"",
                                        connectors :
                                        [
                                        {connectors}
                                        ]
                                    }},";
                }
                result = result.Substring(0, result.Length - 1);
            }
            return result;
        }

        public string GetChargePointForInitiateController(List<ChargePointDetails> chargePointDetails, List<int> listOfvendorId)
        {
            var result = string.Empty;

            if(chargePointDetails?.Count > 0)
            {
                foreach(var vendorId in listOfvendorId)
                {
                    var vendorName = Convert.ToString((EnumVendor)vendorId);

                    foreach (var details in chargePointDetails)
                    {
                        result += $@"{{
                                    disableAuthentication : false,
                                    id : ""{details.ChargerName}"",
                                    ip : ""{details.IP}"",
                                    maxAmpere : {details.MaxAmpere},
                                    vendor : {vendorName}
                                }},";
                    }
                }

                result = result.Substring(0, result.Length - 1);
            }

            return result;
        }

        public void UpdateChargePointInitiatedField(CrmServiceClient service, List<ChargePointDetails> chargePointDetails)
        {
            if (chargePointDetails?.Count > 0)
            {
                foreach (var details in chargePointDetails)
                {
                    Entity charger = new Entity("gk_charger", details.ChargerId);
                    charger["gk_isinitiated"] = true;
                    service.Update(charger);
                }
            }
        }

        public List<ChargePointDetails> GetChargePointDetails(CrmServiceClient service, Guid controllerId, List<string> listOfChargerId = null)
        {
            List<ChargePointDetails> listOfChargerPoint = null;

            var lstChargers = GetChargers(service, controllerId, listOfChargerId);

            if (lstChargers != null && lstChargers.Entities.Count > 0)
            {
                listOfChargerPoint = new List<ChargePointDetails>();

                foreach (var charger in lstChargers.Entities)
                {
                    var model = new ChargePointDetails();
                    model.ChargerId = charger.Id;
                    if (charger.Attributes.Contains("gk_name"))
                    {
                        model.ChargerName = Convert.ToString(charger["gk_name"]);
                    }
                    if (charger.Attributes.Contains("gk_ip"))
                    {
                        model.IP = Convert.ToString(charger["gk_ip"]).Replace("https://", "");
                    }

                    Entity connectors = GetDefaultConnectorByCharger(service, charger.Id);
                    if (connectors != null)
                    {
                        if (connectors.Attributes.Contains("connector.gk_amperageconnector"))
                        {
                            model.MaxAmpere = Convert.ToInt32(((AliasedValue)connectors["connector.gk_amperageconnector"]).Value);
                            if (model.MaxAmpere < 7)
                            {
                                model.MaxAmpere = 32;
                            }
                        }
                        else
                        {
                            model.MaxAmpere = 32;
                        }
                    }

                    listOfChargerPoint.Add(model);
                }
            }

            return listOfChargerPoint;
        }

        public string GetConnectors(Guid chargerId, CrmServiceClient service)
        {
            string result = string.Empty;
            var lstConnectors = GetConnectors(service, chargerId);
            if (lstConnectors != null && lstConnectors.Entities.Count > 0)
            {
                foreach (var connector in lstConnectors.Entities)
                {
                    int connectorLevel = 0;
                    string phaseMapping = string.Empty;
                    if (connector.Attributes.Contains("gk_connectorlevel"))
                    {
                        connectorLevel = Convert.ToInt32(CRMHelper.GetOptionSetText(connector, "gk_connectorlevel"));
                    }
                    if (connector.Attributes.Contains("gk_phasemapping"))
                    {
                        phaseMapping = Convert.ToString(connector["gk_phasemapping"]);
                    }

                    result += $@"{{
                                      id : {connectorLevel},
                                      phaseMapping : ""{phaseMapping}""
                                 }},";
                }
                result = result.Substring(0, result.Length - 1);
            }
            return result;
        }

        public string GetClusterPoints(Guid controllerId, CrmServiceClient service, int fuzeSize, string sensortype)
        {
            string result = string.Empty;
            var lstClusters = GetClusters(service, controllerId);
            if (lstClusters != null && lstClusters.Entities.Count > 0)
            {
                Guid rootClusterId = new Guid();
                foreach (var cluster in lstClusters.Entities)
                {
                    int headroom = 0;
                    string parentCluster = string.Empty;
                    EnumNetType enumNetType = EnumNetType.IT;
                    string clusterId = string.Empty;
                    string phaseMapping = string.Empty;
                    if (cluster.Attributes.Contains("gk_headroom"))
                    {
                        headroom = Convert.ToInt32(cluster["gk_headroom"]);
                    }

                    if (!cluster.Attributes.Contains("gk_parentclusterid"))
                    {
                        rootClusterId = cluster.Id;
                        clusterId = "root";
                    }
                    else
                    {
                        if (cluster.Attributes.Contains("gk_name"))
                        {
                            clusterId = Convert.ToString(cluster["gk_name"]);
                        }

                        EntityReference parentClusterEnityReference = (EntityReference)cluster["gk_parentclusterid"];
                        if (parentClusterEnityReference.Id == rootClusterId)
                        {
                            parentCluster = "root";
                        }
                        else
                        {
                            parentCluster = parentClusterEnityReference.Name;
                        }
                    }

                    if (cluster.Attributes.Contains("gk_nettype"))
                    {
                        int netType = CRMHelper.GetOptionSetValue(cluster.Attributes,"gk_nettype");
                        if (netType == (int)EnumNetType.TN)
                        {
                            enumNetType = EnumNetType.TN;
                            phaseMapping = "N123";
                        }
                        else if (netType == (int)EnumNetType.TT)
                        {
                            enumNetType = EnumNetType.TT;
                            phaseMapping = "xNLx";
                        }
                        else if (netType == (int)EnumNetType.IT)
                        {
                            enumNetType = EnumNetType.IT;
                            phaseMapping = "xNLx";
                        }
                    }

                    if (clusterId != "root")
                    {
                        result += $@"{{
                                        id : ""{clusterId}"",
                                        fuseLimit :{fuzeSize},
                                        headroom : {headroom},
                                        netType : {enumNetType},
                                        parent : ""{parentCluster}"",
                                        _default : 
                                            {{
                                                accessControl : {EnumAccessControlType.CONTROLLER_OPEN},
                                                maxAmpere : 32,
                                                minAmpere : 7,
                                                phaseMapping : ""{phaseMapping}"",
                                                priority : {EnumChargePriority.NORMAL}
                                            }}
                                    }},";
                    }
                    else
                    {
                        result += $@"{{
                                        id : ""{clusterId}"",
                                        fuseLimit :{fuzeSize},
                                        headroom : {headroom},
                                        netType : {enumNetType},
                                        sensor : ""{sensortype}"",
                                        _default : 
                                            {{
                                                accessControl : {EnumAccessControlType.CONTROLLER_OPEN},
                                                maxAmpere : 32,
                                                minAmpere : 7,
                                                phaseMapping : ""{phaseMapping}"",
                                                priority : {EnumChargePriority.NORMAL}
                                            }}
                                    }},";
                    }
                }
                result = result.Substring(0, result.Length - 1);
            }
            return result;
        }

        public void CallSetSensorModbusGraphQL(string serialNumber, string token)
        {
            _logger.StatusMessage(1, $"<div style='margin-top:10px;'>SetSensorModbusController Request execution started.</div>");

            _logger.StatusMessage(1, $"<div>Serial Number : {serialNumber}</div>");
            _telemetryClient.TrackTrace($"Serial Number  {serialNumber}");

            GraphQlQuery graphQl = new GraphQlQuery();
            var query = $@"mutation{{
                            setSensorModbus(input:
                            {{
                                deviceId : ""{serialNumber}"",
                                model : {EnumModel.CARLO_GAVAZZI_EM210},
                                name : ""root"",
                                slaveId : 1
                            }})
                         }}";

            graphQl.Query = query;

            _telemetryClient.TrackTrace($"SetSensorModbus  GraphQL request : {JsonConvert.SerializeObject(graphQl)}");

            var result = GraphQLHelper.GraphQLRequest(graphQl, token);

            _telemetryClient.TrackTrace($"SetSensorModbus  GraphQL result : {result}");

            var resultData = JsonConvert.DeserializeObject<dynamic>(result);

            if (resultData != null && resultData.data != null && Convert.ToString(resultData.data) != "{}")
            {
                _logger.StatusMessage(1, $"<div> SetSensorModbus GraphQL result : {resultData.data}.</div>");
                _logger.StatusMessage(1, "<div>SetSensorModbus GraphQL executes successfully.</div>");
                _telemetryClient.TrackTrace("SetSensorModbus GraphQL executes successfully.");
            }
            else
            {
                if (resultData != null && resultData.errors != null)
                {
                    _telemetryClient.TrackTrace($"SetSensorModbus GraphQL Error {resultData.errors.ToString()}");
                    _logger.StatusMessage(1, $"<div>SetSensorModbus GraphQL Error :</div>");
                    GraphQLErrorLog(Convert.ToString(resultData));
                }

                _logger.StatusMessage(1, "<div> SetSensorModbus GraphQL fail.</div>");
            }
            _logger.StatusMessage(1, "<div>SetSensorModbusController Request execution completed.</div>");
            _telemetryClient.TrackTrace("SetSensorModbusController Request execution completed.");
        }

        public static EntityCollection GetChargers(CrmServiceClient service, Guid controllerId, List<string> listOfChargerId = null)
        {
            var fetchXML = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                                 "<entity name='gk_charger'>" +
                                   "<attribute name='gk_chargerid' />" +
                                   "<attribute name='gk_name' />" +
                                   "<attribute name='gk_serialnumber' />" +
                                   "<attribute name='gk_ip' />" +
                                   "<attribute name='gk_accesscontrol' />" +
                                   "<attribute name='gk_clusterid' />" +
                                   "<filter type='and'>" +
                                       "<condition attribute='statecode' operator='eq' value='0'/>" +
                                        "<condition attribute='gk_controllerid' operator='eq' value='{" + controllerId + "}' />";
            if (listOfChargerId != null && listOfChargerId.Any())
            {
                fetchXML += "<condition attribute='gk_chargerid' operator='in'>";
                foreach (var chargerId in listOfChargerId)
                {
                    fetchXML += $"<value>{chargerId.Trim()}</value>";
                }
                fetchXML += "</condition>";
            }

            fetchXML += "</filter></entity></fetch>";

            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                return entityCollection;
            }
            return null;
        }

        public static Entity GetActiveChargerBySerialId(CrmServiceClient service, string serialNumber, Guid controllerId)
        {
            string fetchXML = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                                  "<entity name='gk_charger'>" +
                                    "<attribute name='gk_name' />" +
                                    "<attribute name='gk_serialnumber' />" +
                                    "<filter type='and'>" +
                                      "<condition attribute='gk_serialnumber' operator='eq' value='" + serialNumber + "' />" +
                                      "<condition attribute='gk_controllerid' operator='eq' value='{" + controllerId + "}' />" +
                                      "<condition attribute='statecode' operator='eq' value='0'/>" +
                                    "</filter>" +
                                  "</entity>" +
                                "</fetch>";
            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchXML));

            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                return entityCollection.Entities[0];
            }

            return null;
        }

        private static EntityCollection GetConnectors(CrmServiceClient service, Guid chargerId)
        {
            string fetchXML = string.Empty;

            fetchXML = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                              "<entity name='gk_connector'>" +
                                "<attribute name='gk_phasemapping' />" +
                                "<attribute name='gk_connectorlevel' />" +
                                "<attribute name='gk_uuid' />" +
                                "<filter type='and'>" +
                                  "<condition attribute='gk_chargerid' operator='eq' value='{" + chargerId + "}' />" +
                                  "<condition attribute='statecode' operator='eq' value='0'/>" +
                                "</filter>" +
                              "</entity>" +
                            "</fetch>";

            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                return entityCollection;
            }

            return null;
        }

        private static EntityCollection GetClusters(CrmServiceClient service, Guid controllerId)
        {
            string fetchXML = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                                  "<entity name='gk_cluster'>" +
                                    "<attribute name='gk_name' />" +
                                    "<attribute name='gk_headroom' />" +
                                    "<attribute name='gk_nettype' />" +
                                    "<attribute name='gk_parentclusterid' />" +
                                    "<filter type='and'>" +
                                      "<condition attribute='gk_controllerid' operator='eq' value='{" + controllerId + "}' />" +
                                      "<condition attribute='statecode' operator='eq' value='0'/>" +
                                    "</filter>" +
                                    "<order attribute='gk_parentclusterid' />" +
                                  "</entity>" +
                                "</fetch>";
            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                return entityCollection;
            }

            return null;
        }

        public static EnumAccessControlType GetEnumAccessControlType(int accessControl, EnumAccessControlType enumAccessControlType)
        {
            if (accessControl == (int)EnumAccessControlType.CONTROLLER_OPEN)
            {
                enumAccessControlType = EnumAccessControlType.CONTROLLER_OPEN;
            }
            else if (accessControl == (int)EnumAccessControlType.CONTROLLER_CLOSED)
            {
                enumAccessControlType = EnumAccessControlType.CONTROLLER_CLOSED;
            }
            else if (accessControl == (int)EnumAccessControlType.CLOSED)
            {
                enumAccessControlType = EnumAccessControlType.CLOSED;
            }
            return enumAccessControlType;
        }

        public Entity GetDefaultConnectorByCharger(CrmServiceClient service, Guid chargerId)
        {
            string fetchXML = string.Empty;
            fetchXML = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                                 "<entity name='gk_charger'>" +
                                   "<attribute name='gk_name' />" +
                                   "<filter type='and'>" +
                                     "<condition attribute='gk_chargerid' operator='eq' value='{" + chargerId + "}' />" +
                                     "<condition attribute='statecode' operator='eq' value='0'/>" +
                                   "</filter>" +
                                   "<link-entity name='gk_connector' from='gk_chargerid' to='gk_chargerid' link-type='inner' alias='connector'>" +
                                        "<attribute name='gk_amperageconnector' />" +
                                      "<filter type='and'>" +
                                            "<condition attribute='gk_connectorlevel' operator='eq' value='1'/>" +
                                            "<condition attribute='statecode' operator='eq' value='0'/>" +
                                      "</filter>" +
                                    "</link-entity>" +
                                 "</entity>" +
                               "</fetch>";
            EntityCollection entityCollection = service.RetrieveMultiple(new FetchExpression(fetchXML));
            if (entityCollection != null && entityCollection.Entities.Count > 0)
            {
                return entityCollection.Entities[0];
            }
            return null;
        }

        public string CreateControllerV2NewToken()
        {
            try
            {
                string grant_type = "client_credentials";

                string scope = AzureHelper.GetValueFromAzureVault("ControllerV2GraphQLScope");
                string clientId = AzureHelper.GetValueFromAzureVault("ControllerV2GraphQLClientId");
                string clientSecret = AzureHelper.GetValueFromAzureVault("ControllerV2GraphQLClientSecret");
                string tenantId = AzureHelper.GetValueFromAzureVault("ControllerV2GraphQLTenantId");
                string tokenURL = ConfigurationManager.AppSettings["ControllerV2GraphQLTokenURL"].ToString();
                tokenURL = String.Format(tokenURL, tenantId);
                var form = new Dictionary<string, string>{
                { "grant_type", grant_type},
                { "client_id", clientId},
                { "client_secret", clientSecret},
                {"scope",scope }
                };

                using (var client = new HttpClient())
                {
                    HttpResponseMessage tokenResponse = client.PostAsync(tokenURL, new FormUrlEncodedContent(form)).Result;
                    if (tokenResponse.IsSuccessStatusCode)
                    {
                        var jsonContent = tokenResponse.Content.ReadAsStringAsync().Result;
                        JToken actualJson = JToken.Parse(jsonContent);
                        string token = actualJson.Value<string>("access_token");

                        _telemetryClient.TrackTrace("New token is generated.");
                        return token;
                    }
                    else
                    {
                        _telemetryClient.TrackTrace($"Error in ControllerV2 oauth2/token API: {tokenResponse.StatusCode} - {tokenResponse.ReasonPhrase}");
                        return string.Empty;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public void GraphQLErrorLog(string resultData)
        {
            GraphQLErrors errorList = JsonConvert.DeserializeObject<GraphQLErrors>(resultData);
            int errorCount = 1;
            foreach (var error in errorList.Errors)
            {
                string errorMessage = string.Empty;
                if (!string.IsNullOrEmpty(error.Message))
                {
                    errorMessage += string.Concat("   ", errorCount.ToString(), ". ", error.Message);
                }

                if (error.Extensions != null && !string.IsNullOrEmpty(error.Extensions.Code))
                {
                    errorMessage += string.Concat(" Code =", error.Extensions.Code, ".");
                }
                if (error.Extensions != null && !string.IsNullOrEmpty(error.Extensions.errorCode))
                {
                    errorMessage += string.Concat(" ErrorCode =", error.Extensions.errorCode);
                }
                _logger.StatusMessage(1, $"<div style='margin-left:20px; margin-top:10px; margin-bottom: 10px;color:red;font-weight:bold;'> {errorMessage} </div>");
                errorCount += 1;
            }
        }
    }
}