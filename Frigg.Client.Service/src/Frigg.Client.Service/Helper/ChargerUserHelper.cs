﻿using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;

namespace Frigg.Client.Service.Helper
{
    public static class ChargerUserHelper
    {
        public static void SetupChargerUser(SetupChargerUserRequestMessage request, TelemetryClient telemetryClient, TransactionLogger logger)
        {
            var connectionString = AzureHelper.GetValueFromAzureVault("DynamicsConnectionWithAppUser");
            var service = CRMHelper.GetCRMService(connectionString + ";RequireNewInstance=true");

            if(service != null && service.IsReady)
            {
                foreach (var data in request.ChargerUsers)
                {
                    logger.StatusMessage(1, "<div>" + GetLogValues(5, $"Charger User with MSISDN {data.Uid} setup is started.") + "</div>");

                    var isExist = IsChargerUserExistWithUid(service, request.DynamicsChargerUserGroupId, data);

                    if (!isExist)
                    {
                        var chargerUserId = CreateChargerUser(service, data, telemetryClient, logger);

                        if(chargerUserId != Guid.Empty)
                        {
                            request.DynamicsChargerUserId = chargerUserId;

                            CreateChargerUserToChargerUserGroup(service, data, request, telemetryClient, logger);
                        }
                    }
                    else
                    {
                        logger.StatusMessage(1, "<div class='error'>" + GetLogValues(10, "Charger User is already exist.") + "</div>");
                    }

                    logger.StatusMessage(1, "<div>" + GetLogValues(5, $"Charger User with MSISDN {data.Uid} setup is completed.") + "</div>");
                    logger.StatusMessage(1, "<br/>");
                }
            }
        }

        private static Guid CreateChargerUser(CrmServiceClient service, ChargerUserDetails data, TelemetryClient telemetryClient, TransactionLogger logger)
        {
            var entity = new Entity("gk_chargeruser");

            entity["gk_name"] = data.Uid;
            entity["gk_msisdn"] = data.Uid;
            entity["gk_accountownerid"] = new EntityReference("account", data.DynamicsAccountId);

            var id = service.Create(entity);

            logger.StatusMessage(1, "<div>" + GetLogValues(10, "Charger User is created.") + "</div>");
            telemetryClient.TrackTrace("Charger User with MSISDN : " + data.Uid + " is created.");

            return id;
        }

        private static void CreateChargerUserToChargerUserGroup(CrmServiceClient service, ChargerUserDetails data, SetupChargerUserRequestMessage request, TelemetryClient telemetryClient, TransactionLogger logger)
        {
            var entity = new Entity("gk_chargerusertochargerusergroup");

            entity["gk_name"] = request.ChargerUserGroupName;
            entity["gk_chargerusergroupid"] = new EntityReference("gk_chargerusergroup", request.DynamicsChargerUserGroupId);
            entity["gk_chargeruserid"] = new EntityReference("gk_chargeruser", request.DynamicsChargerUserId);

            service.Create(entity);

            logger.StatusMessage(1, "<div>" + GetLogValues(10, "Charger User To Charger User Group is created.") + "</div>");
            telemetryClient.TrackTrace($"Charger User To Charger User Group is created. Uid : {data.Uid}");
        }

        private static bool IsChargerUserExistWithUid(CrmServiceClient service, Guid dynamicsId, ChargerUserDetails user)
        {
            var query = new QueryExpression("gk_chargerusertochargerusergroup");
            query.ColumnSet.AddColumns("gk_chargeruserid", "gk_chargerusergroupid");
            query.Criteria.AddCondition("gk_chargerusergroupid", ConditionOperator.Equal, dynamicsId);
            query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);

            var chargerUser = query.AddLink("gk_chargeruser", "gk_chargeruserid", "gk_chargeruserid");
            chargerUser.EntityAlias = "chargerUser";
            chargerUser.LinkCriteria.AddCondition("gk_msisdn", ConditionOperator.Equal, user.Uid);
            chargerUser.LinkCriteria.AddCondition("statecode", ConditionOperator.Equal, 0);

            var collection = service.RetrieveMultiple(query);

            return collection?.Entities?.Count > 0;
        }

        private static string GetLogValues(int spaceLength, string log)
        {
            var space = string.Empty;

            for (var i = 0; i <= spaceLength; i++)
            {
                space += "&#160;";
            }

            return space + log;
        }
    }
}