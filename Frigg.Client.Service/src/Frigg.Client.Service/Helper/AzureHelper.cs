﻿using AzureFunctionKeyVault.Secrets.Data;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using System;
using System.Configuration;
using System.Runtime.Caching;
using System.Text;

namespace Frigg.Client.Service.Helper
{
    public class AzureHelper
    {
        private static KeyVaultClient keyVaultClient = null;
        public static string GetValueFromAzureVault(string secret)
        {
            var azureServiceTokenProvider = new AzureServiceTokenProvider();
            var value = string.Empty;
            var vaultBaseURL = ConfigurationManager.AppSettings["AzureKeyVaultUrl"].ToString();
            try
            {                
                keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));
                var result = ProcessSecret(vaultBaseURL, secret);
                value = result.SecretValue;
            }
            catch (Exception e)
            {
                StringBuilder strErrorDetails = new StringBuilder();
                strErrorDetails.AppendFormat(" Valut Base URL: {0}", vaultBaseURL);

                if (e.InnerException != null && !string.IsNullOrEmpty(e.InnerException.Message))
                    strErrorDetails.AppendFormat(" ,InnerException: {0}", e.InnerException.Message);
                if (!string.IsNullOrEmpty(e.Message))
                    strErrorDetails.AppendFormat(" ,Message: {0}", e.Message);
                if (!string.IsNullOrEmpty(e.StackTrace))
                    strErrorDetails.AppendFormat(" ,StackTrace: {0}", e.StackTrace);

                throw new Exception($"Error occurred in GetValueFromAzureVault. {strErrorDetails.ToString()}", e);
            }

            return value;
        }

        private static SecretResult ProcessSecret(string vaultBaseURL, string secret)
        {
            try
            {
                SecretResult taskGetSecret = GetSecretAsync(vaultBaseURL, secret);
                return taskGetSecret;
            }
            catch (Exception e)
            {
                return new SecretResult { IsSuccessful = false, Status = $"Error occurred in processing secret: {e.Message}" };
            }
        }

        private static SecretResult GetSecretAsync(string vaultBaseURL, string secret)
        {
            try
            {
                var cache = MemoryCache.Default;
                if (!cache.Contains(secret))
                {
                    var vaultSecret = keyVaultClient.GetSecretAsync(vaultBaseURL, secret).Result;
                    if (vaultSecret != null && !string.IsNullOrWhiteSpace(vaultSecret.Value))
                    {
                        cache.Add(secret, vaultSecret.Value, new CacheItemPolicy { SlidingExpiration = TimeSpan.FromHours(1) });
                    }
                }

                var secretValue = cache.Contains(secret) ? cache[secret] as string : "";
                if (string.IsNullOrWhiteSpace(secretValue))
                {
                    return new SecretResult { IsSuccessful = false, Status = "Secret Value not found!" };
                }

                return new SecretResult { IsSuccessful = true, SecretValue = secretValue, Status = "Secret Value retrieved from KeyVault." };
            }
            catch (Exception e)
            {
                return new SecretResult { IsSuccessful = false, Status = $"Error retrieving secret value for {secret}, error: {e.Message}" };
            }
        }
    }
}