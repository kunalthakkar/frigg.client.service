﻿using Frigg.AdyenUtility.Framework;
using Frigg.AdyenUtility.Framework.Data;
using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Helper
{
    public static class KlarnaPaymentHelper
    {
        public static OrderDetails GetOrderDetails(CrmServiceClient service, Guid orderId)
        {
            OrderDetails details = null;
            
            var query = new QueryExpression("gk_order");
            query.ColumnSet.AddColumns("gk_ordernumber", "gk_invoiceamountincvat", "gk_customerid");
            query.Criteria.AddCondition("gk_orderid", ConditionOperator.Equal, orderId);
            query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);
            query.Criteria.AddCondition("gk_transferredtimestamp", ConditionOperator.Null);
            query.Criteria.AddCondition("gk_customerid", ConditionOperator.NotNull);

            var account = query.AddLink("account", "gk_customerid", "accountid");
            account.EntityAlias = "account";
            account.Columns.AddColumns("gk_businessunitid");

            var businessUnit = account.AddLink("businessunit", "gk_businessunitid", "businessunitid");
            businessUnit.EntityAlias = "businessUnit";
            businessUnit.Columns.AddColumns("gk_basecurrencyid");

            var currency = businessUnit.AddLink("gk_currencygk", "gk_basecurrencyid", "gk_currencygkid");
            currency.EntityAlias = "currency";
            currency.Columns.AddColumns("gk_currencycode");

            var collection = service.RetrieveMultiple(query);

            if(collection?.Entities?.Count > 0)
            {
                var entity = collection.Entities[0];

                details = new OrderDetails
                {
                    OrderId = orderId,
                    AmountIncVat = Convert.ToDouble(CRMHelper.GetDecimalValue(entity.Attributes, "gk_invoiceamountincvat")),
                    OrderNumber = CRMHelper.GetTextValue(entity.Attributes, "gk_ordernumber"),
                    Currency = CRMHelper.GetTextValue(entity.Attributes, "currency.gk_currencycode", true)
                };
            }

            return details;
        }

        public static string GetPspReference(CrmServiceClient service, Guid orderId)
        {
            var pspReerece = string.Empty;

            var query = new QueryExpression("gk_cardtransaction");
            query.ColumnSet.AddColumns("gk_orderid", "gk_captureattempt", "gk_name", "gk_capturemessage");
            query.Criteria.AddCondition("gk_orderid", ConditionOperator.Equal, orderId);
            query.Criteria.AddCondition("gk_name", ConditionOperator.Like, "%PspReference%");

            var collection = service.RetrieveMultiple(query);

            if (collection?.Entities?.Count > 0)
            {
                var entity = collection.Entities[0];

                if (entity.Attributes.Contains("gk_capturemessage"))
                {
                    var captureMessage = CRMHelper.GetTextValue(entity.Attributes, "gk_capturemessage");

                    JObject data = JObject.Parse(captureMessage);
                    pspReerece = data.SelectToken("pspReference")?.ToString();
                }
            }

            return pspReerece;
        }

        public static void ProcessKlarnaCapturePayment(CrmServiceClient service, OrderDetails orderDetails, 
                    TelemetryClient telemetryClient, TransactionLogger logger)
        {
            var apiKey = AzureHelper.GetValueFromAzureVault("AdyenApiKey");
            var merchantAccount = AzureHelper.GetValueFromAzureVault("AdyenMerchantAccount");
            var adyenPaymentUrl = ConfigurationManager.AppSettings["AdyenPaymentUrl"].ToString() + orderDetails.PspReference + "/captures";

            IAdyenProcessor adyenProcessor = new AdyenProcessor(apiKey, adyenPaymentUrl, merchantAccount);

            var request = new KlarnaCapturePaymentRequestData
            {
                Amount = new Amount
                {
                    Currency = orderDetails.Currency,
                    Value = orderDetails.AmountIncVat * 100
                },
                Reference = orderDetails.OrderNumber,
                MerchantAccount = merchantAccount
            };

            telemetryClient.TrackTrace($"Adyen request : {JsonConvert.SerializeObject(request)}");

            var adyenResult = adyenProcessor.ProcessTransaction(request, TypeOfRequest.KlarnaCapturePayment);

            telemetryClient.TrackTrace($"Adyen result for invoice {orderDetails.OrderNumber} : {JsonConvert.SerializeObject(adyenResult)}");

            ProcessAdyenResult(service, adyenResult, orderDetails, logger, telemetryClient);
        }

        public static string GetLogValues(int spaceLength, string log)
        {
            var space = string.Empty;

            for (var i = 0; i <= spaceLength; i++)
            {
                space += "&#160;";
            }

            return space + log;
        }

        private static void ProcessAdyenResult(CrmServiceClient service, AdyenResponse adyenResult, OrderDetails orderDetails, TransactionLogger logger, TelemetryClient telemetryClient)
        {
            var attempts = GetTransactionAttempts(service, orderDetails.OrderId) + 1;

            if (adyenResult.Status == "Success")
            {
                JObject data = JObject.Parse(adyenResult.Data);
                var resultCode = data.SelectToken("status")?.ToString();

                var transaction = new Entity("gk_cardtransaction");

                transaction["gk_orderid"] = new EntityReference("gk_order", orderDetails.OrderId);
                transaction["gk_captureattempt"] = attempts;
                transaction["gk_name"] = resultCode;
                transaction["gk_capturemessage"] = adyenResult.Data;

                service.Create(transaction);

                if (!string.IsNullOrEmpty(resultCode) && resultCode.ToLower().Equals("received"))
                {
                    var updatedInvoice = new Entity("gk_order")
                    {
                        Id = orderDetails.OrderId
                    };

                    updatedInvoice["gk_transferredtimestamp"] = DateTime.UtcNow;
                    updatedInvoice["statecode"] = new OptionSetValue(1);
                    updatedInvoice["statuscode"] = new OptionSetValue(2);

                    service.Update(updatedInvoice);

                    logger.StatusMessage(1, "<div>" + GetLogValues(15, "Invoice is Paid.") + "</div>");
                    telemetryClient.TrackTrace($"Invoice {orderDetails.OrderNumber} is Paid. ");
                }
                else
                {
                    logger.StatusMessage(1, "<div class='error'>" + GetLogValues(15, "Invoice is not paid due to Adyen error.") + "</div>");
                    logger.StatusMessage(1, "<div>" + GetLogValues(10, adyenResult.Data) + "</div><br/>");
                }
            }
            else
            {
                var warning = new Entity("gk_cardtransaction");

                warning["gk_name"] = adyenResult.Message;
                warning["gk_capturemessage"] = adyenResult.Data;
                warning["gk_captureattempt"] = attempts;
                warning["gk_orderid"] = new EntityReference("gk_order", orderDetails.OrderId);

                service.Create(warning);

                logger.StatusMessage(1, "<div class='error'>" + GetLogValues(15, "Invoice is not paid due to Adyen error.") + "</div>");
                logger.StatusMessage(1, "<div>" + GetLogValues(10, adyenResult.Data) + "</div><br/>");

                telemetryClient.TrackTrace($"Invoice {orderDetails.OrderNumber} is not paid due to Adyen error.");
            }

            logger.StatusMessage(1, "<div>" + GetLogValues(10, "Adyen klarna payment process is completed.") + "</div>");
        }

        private static int GetTransactionAttempts(CrmServiceClient service, Guid orderId)
        {
            var totalCount = 0;

            var query = new QueryExpression("gk_cardtransaction");

            query.ColumnSet = new ColumnSet("gk_orderid", "statecode");
            query.Criteria.AddCondition("gk_orderid", ConditionOperator.Equal, orderId);
            query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);

            var collection = service.RetrieveMultiple(query);

            if (collection?.Entities?.Count > 0)
            {
                totalCount += collection.Entities.Count;
            }

            return totalCount;
        }
    }
}