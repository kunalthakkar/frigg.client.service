﻿using Frigg.Client.Service.Models;
using Microsoft.ApplicationInsights;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Http;
using System.Text;

namespace Frigg.Client.Service.Helper
{
    public class ChargingFacilityHelper
    {
        private readonly TransactionLogger _logger;
        private readonly TelemetryClient _telemetryClient;
        public static string connectionString;

        public ChargingFacilityHelper(TransactionLogger logger, TelemetryClient telemetryClient)
        {
            _logger = logger;
            _telemetryClient = telemetryClient;
        }

        public ChargingFacilityResponse ChargerStart(ChargingFacilityRequestMessage request)
        {
            _logger.StatusMessage(1, "<div><b>RemoteStart Request execution Started</b></div>");
            _telemetryClient.TrackTrace("RemoteStart Request execution Started.");

            connectionString = AzureHelper.GetValueFromAzureVault("SQLConnectionFriggCommandDB");
            Guid transactionId = Guid.NewGuid();
            InsertStartSession(request, transactionId);
            _telemetryClient.TrackTrace("New record inserted in StartSession Table.");


            string requestUrl = ConfigurationManager.AppSettings["ChargerStartStopAPI"].ToString() + "api/ChargerStart";

            _telemetryClient.TrackTrace($"RemoteStart Request URL: {requestUrl}");

            var requestModel = new
            {
                tagId = request.TagId,
                evseId = request.EvseId,
                transactionId = transactionId
            };
            var requestBody = JsonConvert.SerializeObject(requestModel);

            _logger.StatusMessage(1, "<div><br /></div><div><b><div><b>RemoteStart Request body: </b></div>");
            _logger.StatusMessage(1, "<div>" + requestBody + "</div>");
            _telemetryClient.TrackTrace($"RemoteStart Request body: {requestBody}");

            using (HttpClient client = new HttpClient())
            {
                var inputJObject = new StringContent(requestBody, Encoding.UTF8, "application/json");
                var response = client.PostAsync(requestUrl, inputJObject).Result;
                string responseString = response.Content.ReadAsStringAsync().Result;

                _logger.StatusMessage(1, "<div><br /></div><div><b><div><b>RemoteStart Response: </b></div>");
                _logger.StatusMessage(1, "<div>" + responseString + "</div>");
                _telemetryClient.TrackTrace($"RemoteStart response: {responseString}");

                return JsonConvert.DeserializeObject<ChargingFacilityResponse>(responseString);
            }
        }

        public ChargingFacilityResponse ChargerStop(ChargingFacilityRequestMessage request)
        {
            _logger.StatusMessage(1, "<div><b>RemoteStop Request execution Started</b></div>");
            _telemetryClient.TrackTrace("RemoteStop Request execution Started.");

            connectionString = AzureHelper.GetValueFromAzureVault("SQLConnectionFriggCommandDB");
            var transactionIdString = GetLatestSession(request);
            if (!string.IsNullOrEmpty(transactionIdString))
            {
                _telemetryClient.TrackTrace($"Latest SessionId: {transactionIdString}");
                Guid transactionId = new Guid(transactionIdString);

                string requestUrl = ConfigurationManager.AppSettings["ChargerStartStopAPI"].ToString() + "api/ChargerStop";

                _telemetryClient.TrackTrace($"RemoteStop Request URL: {requestUrl}");

                var requestModel = new
                {
                    tagId = request.TagId,
                    evseId = request.EvseId,
                    transactionId = transactionId
                };
                var requestBody = JsonConvert.SerializeObject(requestModel);

                _logger.StatusMessage(1, "<div><br /></div><div><b><div><b>RemoteStop Request body: </b></div>");
                _logger.StatusMessage(1, "<div>" + requestBody + "</div>");
                _telemetryClient.TrackTrace($"RemoteStop Request body: {requestBody}");

                using (HttpClient client = new HttpClient())
                {
                    var inputJObject = new StringContent(requestBody, Encoding.UTF8, "application/json");
                    var response = client.PostAsync(requestUrl, inputJObject).Result;
                    string responseString = response.Content.ReadAsStringAsync().Result;

                    _logger.StatusMessage(1, "<div><br /></div><div><b><div><b>RemoteStop Response: </b></div>");
                    _logger.StatusMessage(1, "<div>" + responseString + "</div>");
                    _telemetryClient.TrackTrace($"RemoteStop response: {responseString}");

                    return JsonConvert.DeserializeObject<ChargingFacilityResponse>(responseString);
                }
            }
            else
            {
                string statusMessage = $"StartSessionId not found for EVSEId: {request.EvseId} & TagId: {request.TagId}";

                _telemetryClient.TrackTrace(statusMessage);

                return new ChargingFacilityResponse { Success = false, ErrorCode = statusMessage };
            }
        }

        private void InsertStartSession(ChargingFacilityRequestMessage request, Guid startSessionId)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
              
                var queryString = $@"INSERT INTO StartSessions(EvseId, TagId, StartSessionId, CreatedOn, AuthId, Issuer) 
                                    VALUES(@EvseId, @TagId, @StartSessionId, @CreatedOn, @AuthId, @Issuer)";
                using (SqlCommand cmd = new SqlCommand(queryString, conn))
                {
                    cmd.Parameters.AddWithValue("@EvseId", request.EvseId);
                    cmd.Parameters.AddWithValue("@TagId", request.TagId);
                    cmd.Parameters.AddWithValue("@StartSessionId", startSessionId);
                    cmd.Parameters.AddWithValue("@CreatedOn", DateTime.UtcNow);
                    cmd.Parameters.AddWithValue("@AuthId", request.TagId);
                    cmd.Parameters.AddWithValue("@Issuer", "GK");

                    cmd.ExecuteNonQuery();
                }
            }
        }

        private string GetLatestSession(ChargingFacilityRequestMessage request)
        {
            string startSessionId = string.Empty;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                var queryString = $@"SELECT TOP 1 StartSessionId FROM STARTSESSIONS WITH (NOLOCK) WHERE EvseId = @EvseId AND TagId = @TagId ORDER BY CreatedOn DESC";
                using (SqlCommand cmd = new SqlCommand(queryString, conn))
                {
                    cmd.Parameters.AddWithValue("@EvseId", request.EvseId);
                    cmd.Parameters.AddWithValue("@TagId", request.TagId);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            reader.Read();
                            if (reader["StartSessionId"] != DBNull.Value)
                            {
                                startSessionId = Convert.ToString(reader["StartSessionId"]);
                            }
                        }
                    }
                    return startSessionId;
                }
            }
        }
    }
}