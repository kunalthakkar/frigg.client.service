﻿using Frigg.Client.Service.Policy;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using System;
using System.Configuration;
using System.Web.Http;

[assembly: OwinStartup(typeof(Frigg.Client.Service.Startup))]
namespace Frigg.Client.Service
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var configuration = new HttpConfiguration();
            WebApiConfig.Register(configuration);

            app.Map("/signalr", map =>
            {
                var corsOptions = new CorsOptions();
                corsOptions.PolicyProvider = new CustomCorsPolicy();

                map.UseCors(corsOptions);
                var hubConfiguration = new HubConfiguration
                {
                    EnableDetailedErrors = true
                };

                map.RunSignalR(hubConfiguration);
            });

            app.UseWebApi(configuration);
        }
    }
}