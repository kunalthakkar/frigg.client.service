﻿using Frigg.Client.Service.Helper;
using Frigg.Client.Service.Hubs;
using Frigg.Client.Service.Models;
using Microsoft.ApplicationInsights;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Frigg.Client.Service.Controllers
{
    [RoutePrefix("api")]
    public class ChargingFacilityController : ApiControllerWithHub<ChargingFacilityControllerHub>
    {
        private readonly TransactionLogger logger;
        private readonly TelemetryClient telemetryClient;
        ChargingFacilityHelper chargingFacilityHelper = null;


        public ChargingFacilityController()
        {
            logger = new TransactionLogger();
            logger.OnStatusMessage += (sender, level, message) => Hub.Clients.All.message(message, level);
            logger.ProgressChanged += (sender, progress) => Hub.Clients.All.progress(progress);
            telemetryClient = Helper.Helper.GetTelemetryClient();
            chargingFacilityHelper = new ChargingFacilityHelper(logger, telemetryClient);
        }

        [HttpPost]
        [Route("ChargerStart")]
        public HttpResponseMessage ChargerStart(ChargingFacilityRequestMessage request)
        {
            try
            {
                telemetryClient.TrackTrace($"ChargerStart API Request  {JsonConvert.SerializeObject(request)}");

                var result = chargingFacilityHelper.ChargerStart(request);

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(result), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    logger.StatusMessage(1, ex.Message);
                    telemetryClient.TrackException(ex);
                }

                if (!string.IsNullOrEmpty(ex.StackTrace))
                    logger.StatusMessage(1, ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        [HttpPost]
        [Route("ChargerStop")]
        public HttpResponseMessage ChargerStop(ChargingFacilityRequestMessage request)
        {
            try
            {
                telemetryClient.TrackTrace($"ChargerStope API Request  {JsonConvert.SerializeObject(request)}");

                var result = chargingFacilityHelper.ChargerStop(request);

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonConvert.SerializeObject(result), Encoding.UTF8, "application/json")
                };
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    logger.StatusMessage(1, ex.Message);
                    telemetryClient.TrackException(ex);
                }

                if (!string.IsNullOrEmpty(ex.StackTrace))
                    logger.StatusMessage(1, ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
