﻿using Frigg.AdyenUtility.Framework;
using Frigg.Client.Service.Helper;
using Frigg.Client.Service.Hubs;
using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Frigg.Client.Service.Controllers
{
    [RoutePrefix("api")]
    public class KlarnaPaymentController : ApiControllerWithHub<KlarnaPaymentHub>
    {
        IOrganizationService service;
        private readonly TransactionLogger logger;
        private readonly TelemetryClient telemetryClient;

        public KlarnaPaymentController()
        {
            logger = new TransactionLogger();
            telemetryClient = Helper.Helper.GetTelemetryClient();
            logger.OnStatusMessage += (sender, level, message) => Hub.Clients.All.message(message, level);
            logger.ProgressChanged += (sender, progress) => Hub.Clients.All.progress(progress);
        }


        [HttpPost]
        [Route("klarnapayment/capture")]
        public HttpResponseMessage KlarnaCapturePayment(KlarnaCapureEventRequestMessage request)
        {
            try
            {
                telemetryClient.TrackTrace($"KlarnaCapturePayment request : {JsonConvert.SerializeObject(request)}");

                var connectionString = Helper.AzureHelper.GetValueFromAzureVault("DynamicsConnectionWithAppUser");
                var service = CRMHelper.GetCRMService(connectionString + ";RequireNewInstance=true");

                var orderDetails = KlarnaPaymentHelper.GetOrderDetails(service, request.OrderId);

                if(orderDetails != null)
                {
                   logger.StatusMessage(1, "<div>" + KlarnaPaymentHelper.GetLogValues(5, $"Process started for Invoice : {orderDetails.OrderNumber}") + "</div>");

                    var pspReference = KlarnaPaymentHelper.GetPspReference(service, request.OrderId);

                    if (!string.IsNullOrEmpty(pspReference))
                    {
                        orderDetails.PspReference = pspReference;

                        logger.StatusMessage(1, "<div>" + KlarnaPaymentHelper.GetLogValues(10, "Adyen klarna payment process is started.") + "</div>");
                        KlarnaPaymentHelper.ProcessKlarnaCapturePayment(service, orderDetails, telemetryClient, logger);

                        return Request.CreateResponse(HttpStatusCode.OK, new KlarnaCaptureEventResponseMessage { Success = true });
                    }
                    else
                    {
                        telemetryClient.TrackTrace("Psp Reference is not found.");
                        logger.StatusMessage(1, "<div>" + KlarnaPaymentHelper.GetLogValues(5, "PspReference is not found.") + "</div>");

                        return Request.CreateResponse(HttpStatusCode.NotFound, new KlarnaCaptureEventResponseMessage { Success = false });
                    }
                }

                return Request.CreateResponse(HttpStatusCode.Forbidden, new KlarnaCaptureEventResponseMessage { Success = false });
            }
            catch(Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                    logger.StatusMessage(1, ex.Message);

                if (!string.IsNullOrEmpty(ex.StackTrace))
                    logger.StatusMessage(1, ex.StackTrace);

                telemetryClient.TrackException(ex, new Dictionary<string, string>
                {
                    {"APIName",  "KlarnaCaptureEvent" },
                    {"Message", ex.Message },
                    {"StackTrace ", ex.StackTrace}
                });

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
