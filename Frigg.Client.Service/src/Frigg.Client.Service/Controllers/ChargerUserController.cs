﻿using Frigg.Client.Service.Helper;
using Frigg.Client.Service.Hubs;
using Frigg.Client.Service.Models;
using Microsoft.ApplicationInsights;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Frigg.Client.Service.Controllers
{
    [RoutePrefix("api")]
    public class ChargerUserController : ApiControllerWithHub<ChargerUserHub>
    {
        private readonly TransactionLogger logger;
        private readonly TelemetryClient telemetryClient;

        public ChargerUserController()
        {
            logger = new TransactionLogger();
            telemetryClient = Helper.Helper.GetTelemetryClient();
            logger.OnStatusMessage += (sender, level, message) => Hub.Clients.All.message(message, level);
            logger.ProgressChanged += (sender, progress) => Hub.Clients.All.progress(progress);
        }

        [HttpPost]
        [Route("SetupChargerUser")]
        public HttpResponseMessage SetupChargerUser(SetupChargerUserRequestMessage request)
        {
            try
            {
                telemetryClient.TrackTrace($"SetupChargerUser request : {JsonConvert.SerializeObject(request)}");

                ChargerUserHelper.SetupChargerUser(request, telemetryClient, logger);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                    logger.StatusMessage(1, ex.Message);

                if (!string.IsNullOrEmpty(ex.StackTrace))
                    logger.StatusMessage(1, ex.StackTrace);

                telemetryClient.TrackException(ex, new Dictionary<string, string>
                {
                    {"APIName",  "SetupChargerUser" },
                    {"Message", ex.Message },
                    {"StackTrace ", ex.StackTrace}
                });

                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
