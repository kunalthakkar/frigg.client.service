﻿using Frigg.Client.Service.Helper;
using Frigg.Client.Service.Hubs;
using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Xrm.Sdk.Query;
using System.Threading.Tasks;

namespace Frigg.Client.Service.Controllers
{
    [RoutePrefix("api")]
    public class RebootChargerController : ApiControllerWithHub<ControllerConfigControllerHub>
    {
        private readonly TransactionLogger logger;
        private readonly TelemetryClient telemetryClient;


        public RebootChargerController()
        {
            logger = new TransactionLogger();
            logger.OnStatusMessage += (sender, level, message) => Hub.Clients.All.message(message, level);
            logger.ProgressChanged += (sender, progress) => Hub.Clients.All.progress(progress);
            telemetryClient = Helper.Helper.GetTelemetryClient();
        }

        [HttpPost]
        [Route("RebootCharger")]
        public HttpResponseMessage RebootCharger(ControllerRebootChargerRequestMessage request)
        {
            try
            {
                telemetryClient.TrackTrace($"RebootChargers API Request  {JsonConvert.SerializeObject(request)}");
                logger.StatusMessage(1, $"<div>Reboot Chargers Request execution started.</div>");
                var connectionString = Helper.AzureHelper.GetValueFromAzureVault("DynamicsConnectionWithAppUser");
                var service = CRMHelper.GetCRMService(connectionString + ";RequireNewInstance=true");

                Guid reqChargerId = new Guid(request.ChargerId);
                Entity chargerEntity = service.Retrieve("gk_charger", reqChargerId, new ColumnSet("gk_endpoint", "gk_endpointtype", "gk_chargepointname"));

                if (chargerEntity != null)
                {
                    string endpointType = chargerEntity.Attributes.Contains("gk_endpointtype") ? Convert.ToString(chargerEntity.Attributes["gk_endpointtype"]) : string.Empty;
                    if (chargerEntity.Attributes.Contains("gk_endpointtype") && Convert.ToString(chargerEntity.Attributes["gk_endpointtype"]) == "GraphQL")
                    {
                        Guid controllerId = new Guid(request.ControllerId);
                        Entity controller = service.Retrieve("gk_controller", controllerId, new ColumnSet("gk_serialnumber"));
                        if (controller != null)
                        {
                            if (controller.Attributes.Contains("gk_serialnumber"))
                            {
                                string serialNumber = Convert.ToString(controller.Attributes["gk_serialnumber"]);
                                logger.StatusMessage(1, $"<div>Serial Number : {serialNumber}</div>");
                                telemetryClient.TrackTrace($"Serial Number  {serialNumber}");
                                string token = new GraphQLHelper(logger, telemetryClient).CreateControllerV2NewToken();

                                if (request.IsSelectAll)
                                {
                                    var chargerEntityCollection = GraphQLHelper.GetChargers(service, controllerId);
                                    if (chargerEntityCollection != null && chargerEntityCollection.Entities.Count > 0)
                                    {
                                        foreach (Entity charger in chargerEntityCollection.Entities)
                                        {
                                            string chargePointId = string.Empty;
                                            if (charger.Attributes.Contains("gk_name"))
                                            {
                                                chargePointId = Convert.ToString(charger["gk_name"]);

                                                logger.StatusMessage(1, $"<div><br /></div><div>ChargePoint Id : {chargePointId}</div>");
                                                telemetryClient.TrackTrace($"ChargePoint Id  {chargePointId}");

                                                CallGraphQL(serialNumber, chargePointId, token);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        logger.StatusMessage(1, "<div> Charger not found for Controller = " + controllerId + "</div>");
                                        telemetryClient.TrackTrace($"Charger not found for Controller = {controllerId}");
                                    }
                                }
                                else
                                {
                                    var chargerIds = request.ChargerId.Split(',');
                                    foreach (var chargerId in chargerIds)
                                    {
                                        string chargePointId = string.Empty;
                                        Entity charger = service.Retrieve("gk_charger", new Guid(chargerId), new ColumnSet("gk_name"));
                                        if (charger != null && charger.Attributes.Contains("gk_name"))
                                        {
                                            chargePointId = Convert.ToString(charger["gk_name"]);

                                            logger.StatusMessage(1, $"<div><br /></div><div>ChargePoint Id : {chargePointId}</div>");
                                            telemetryClient.TrackTrace($"ChargePoint Id  {chargePointId}");

                                            CallGraphQL(serialNumber, chargePointId, token);
                                        }
                                        else
                                        {
                                            logger.StatusMessage(1, "<div> Charger not found.</div>");
                                            telemetryClient.TrackTrace("Charger not found.");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                logger.StatusMessage(1, "<div> Serial Number not found.</div>");
                                telemetryClient.TrackTrace("Serial Number not found.");
                            }
                        }
                        else
                        {
                            logger.StatusMessage(1, "<div> Controller not found.</div>");
                            telemetryClient.TrackTrace("Controller not found.");
                        }
                    }
                    else if (chargerEntity.Attributes.Contains("gk_endpoint"))
                    {
                        ControllerV1Reboot(chargerEntity);
                    }
                    else
                    {
                        logger.StatusMessage(1, "<div style='color:red;'> Please select either ControllerV1 or ControllerV2 Charger </div>");
                        telemetryClient.TrackTrace("Please select either ControllerV1 or ControllerV2 Charger.");
                    }
                }
                else
                {
                    logger.StatusMessage(1, "<div style='color:red;'> Please select either ControllerV1 or ControllerV2 Charger </div>");
                    telemetryClient.TrackTrace("Please select either ControllerV1 or ControllerV2 Charger.");
                }
                logger.StatusMessage(1, "<div><br /></div><div>Reboot Chargers Request execution completed.</div>");
                telemetryClient.TrackTrace("Reboot Chargers Request execution completed.");
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    logger.StatusMessage(1, ex.Message);
                    telemetryClient.TrackException(ex);
                }

                if (!string.IsNullOrEmpty(ex.StackTrace))
                    logger.StatusMessage(1, ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        private void CallGraphQL(string deviceId, string chargePointId, string token)
        {
            GraphQlQuery graphQl = new GraphQlQuery();
            var query = $@"mutation{{
                            rebootChargePoint(
                            deviceId: ""{deviceId}"",
                            chargePointId: ""{chargePointId}""
                            )
                         }}";

            graphQl.Query = query;

            telemetryClient.TrackTrace($"GraphQL ChargerReboot Request Body: {graphQl.Query}");

            var result = GraphQLHelper.GraphQLRequest(graphQl, token);

            var resultData = JsonConvert.DeserializeObject<dynamic>(result);

            if (resultData.data != null && Convert.ToString(resultData.data) != "{}")
            {
                logger.StatusMessage(1, "<div style='margin-top:10px;'> GraphQL ChargerReboot Response:</div>");
                logger.StatusMessage(1, "<div>" + Convert.ToString(resultData.data) + "</div>");
                telemetryClient.TrackTrace($"GraphQL ChargerReboot Response: {resultData.data}");
            }
            else
            {
                if (resultData.errors != null)
                {
                    telemetryClient.TrackTrace($"GraphQL ChargerReboot Error {Convert.ToString(resultData.errors)}");

                    logger.StatusMessage(1, $"<div style='margin-top:10px;'>GraphQL ChargerReboot Error:</div>");


                    new GraphQLHelper(logger, telemetryClient).GraphQLErrorLog(Convert.ToString(resultData));
                }

                logger.StatusMessage(1, "<div>Reboot Chargers fail.</div>");
            }
        }


        private void ControllerV1Reboot(Entity charger)
        {
            try
            {
                string apiURL = Convert.ToString(charger.Attributes["gk_endpoint"]).Replace("$1", string.Empty);
                string identifier = charger.Attributes.Contains("gk_chargepointname") ? Convert.ToString(charger.Attributes["gk_chargepointname"]) : string.Empty;

                using (var controllerClient = new HttpClient())
                {
                    controllerClient.BaseAddress = new Uri(apiURL);
                    ControllerV1RebootRequestMessage controllerV1RebootRequestMessage = new ControllerV1RebootRequestMessage();
                    controllerV1RebootRequestMessage.messageType = "rebootChargePointRequest";
                    controllerV1RebootRequestMessage.transactionId = Guid.NewGuid();
                    controllerV1RebootRequestMessage.identifier =  identifier;
                    controllerV1RebootRequestMessage.rebootAll = false;

                    HttpResponseMessage res = controllerClient.PostAsJsonAsync($"Reboot", controllerV1RebootRequestMessage).Result;

                    logger.StatusMessage(1, $"<div><br/></div><div>ControllerV1Reboot Request API :  <div><br/></div> {res.RequestMessage.RequestUri.OriginalString} </div>");

                    logger.StatusMessage(1, $"<div><br/></div><div>Payload :  <div><br/></div>  {JsonConvert.SerializeObject(controllerV1RebootRequestMessage)} </div>");

                    telemetryClient.TrackTrace($"ControllerV1Reboot Request  :  {JsonConvert.SerializeObject(controllerV1RebootRequestMessage)}");

                    var responseString = res.Content.ReadAsStringAsync().Result;
                    if (res.IsSuccessStatusCode)
                    {
                        using (HttpContent content = res.Content)
                        {
                            logger.StatusMessage(1, $"<div><br/></div><div> Reboot Response : <div><br/></div>" + responseString + "</div>");
                            telemetryClient.TrackTrace($"ControllerV1Reboot Response: {responseString}");
                        }
                    }
                    else
                    {
                        logger.StatusMessage(1, $"<div><br/></div><div style='color:red;> Reboot Response :  <div><br/></div> {responseString} </div>");
                        telemetryClient.TrackTrace($"Reboot Response : {responseString}");
                    }
                }

            }
            catch (Exception ex)
            {
                logger.StatusMessage(1, $"<div><br/></div><div style='color:red;>Error Response :  <div><br/></div> {ex.Message} </div>");
                telemetryClient.TrackTrace($"Error Response :{ex.Message}");
                throw;
            }
        }

    }
}
