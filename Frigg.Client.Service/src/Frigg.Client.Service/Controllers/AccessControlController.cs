﻿using Frigg.Client.Service.Helper;
using Frigg.Client.Service.Hubs;
using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Services.Description;

namespace Frigg.Client.Service.Controllers
{
    [RoutePrefix("api")]
    public class AccessControlController : ApiControllerWithHub<AccessControlHub>
    {
        private readonly TransactionLogger logger;
        private readonly TelemetryClient telemetryClient;

        public AccessControlController()
        {
            logger = new TransactionLogger();
            logger.OnStatusMessage += (sender, level, message) => Hub.Clients.All.message(message, level);
            logger.ProgressChanged += (sender, progress) => Hub.Clients.All.progress(progress);
            telemetryClient = Helper.Helper.GetTelemetryClient();
        }

        [HttpPost]
        [Route("AccessControl")]
        public HttpResponseMessage AccessControl(ConfigureChargePointRequestMessage request)
        {
            try
            {
                telemetryClient.TrackTrace($"ConfigureChargePoint API Request  {JsonConvert.SerializeObject(request)}");
                logger.StatusMessage(1, $"<div>Configure ChargePoint Request execution started.</div>");

                var connectionString = Helper.AzureHelper.GetValueFromAzureVault("DynamicsConnectionWithAppUser");
                var service = CRMHelper.GetCRMService(connectionString + ";RequireNewInstance=true");

                Guid reqChargerId = new Guid(request.ActualChargerId);
                Entity chargerEntity = service.Retrieve("gk_charger", reqChargerId, new ColumnSet("gk_endpoint", "gk_endpointtype", "gk_chargepointname"));

                if (chargerEntity != null)
                {
                    string endpointType = chargerEntity.Attributes.Contains("gk_endpointtype") ? Convert.ToString(chargerEntity.Attributes["gk_endpointtype"]) : string.Empty;
                    if (chargerEntity.Attributes.Contains("gk_endpointtype") && Convert.ToString(chargerEntity.Attributes["gk_endpointtype"]) == "GraphQL")
                    {
                        if (!string.IsNullOrEmpty(request.ChargerId) && !string.IsNullOrEmpty(request.DeviceId) && !string.IsNullOrEmpty(request.AccessControl))
                        {
                            string token = new GraphQLHelper(logger, telemetryClient).CreateControllerV2NewToken();
                            CallGraphQL(request, token);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(request.ChargerId))
                            {
                                logger.StatusMessage(1, "<div> ChargerId not found.</div>");
                                telemetryClient.TrackTrace("ChargerId not found.");
                            }
                            else if (string.IsNullOrEmpty(request.DeviceId))
                            {
                                logger.StatusMessage(1, "<div> DeviceId not found.</div>");
                                telemetryClient.TrackTrace("DeviceId not found.");
                            }
                            else if (string.IsNullOrEmpty(request.AccessControl))
                            {
                                logger.StatusMessage(1, "<div> AccessControl not found.</div>");
                                telemetryClient.TrackTrace("AccessControl not found.");
                            }
                        }
                    }
                    else if (chargerEntity.Attributes.Contains("gk_endpoint") && (request.AccessControl == ((int)EnumAccessControlType.OPEN).ToString() || request.AccessControl == ((int)EnumAccessControlType.CLOSED).ToString() || request.AccessControl == ((int)EnumAccessControlType.CONTROLLER_OPEN).ToString()))
                    {
                        ControllerV1AccessControl(chargerEntity, request.AccessControl);
                    }
                    else
                    {
                        logger.StatusMessage(1, "<div style='color:red;'> Please select either ControllerV1 or ControllerV2 Charger </div>");
                        telemetryClient.TrackTrace("Please select either ControllerV1 or ControllerV2 Charger.");
                    }
                }
                else
                {
                    logger.StatusMessage(1, "<div style='color:red;'> Please select either ControllerV1 or ControllerV2 Charger </div>");
                    telemetryClient.TrackTrace("Please select either ControllerV1 or ControllerV2 Charger.");
                }
                logger.StatusMessage(1, "<div><br /></div><div>Configure ChargePoint Request execution completed.</div>");
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    logger.StatusMessage(1, ex.Message);
                    telemetryClient.TrackException(ex);
                }

                if (!string.IsNullOrEmpty(ex.StackTrace))
                    logger.StatusMessage(1, ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        private void CallGraphQL(ConfigureChargePointRequestMessage request, string token)
        {
            EnumAccessControlType enumAccessControlType = EnumAccessControlType.OPEN;
            enumAccessControlType = GraphQLHelper.GetEnumAccessControlType(Convert.ToInt32(request.AccessControl), enumAccessControlType);

            GraphQlQuery graphQl = new GraphQlQuery();
            var query = $@"mutation{{
                            configureChargePointAccess(input:
                            {{
                            deviceId: ""{request.DeviceId}"",
                            id: ""{request.ChargerId}"",
                            accessControl: {enumAccessControlType}
                            }})
                         }}";

            graphQl.Query = query;

            telemetryClient.TrackTrace($"GraphQL ConfigureChargePoint Request Body: {graphQl.Query}");
            logger.StatusMessage(1, $"<div><br /> GraphQL ConfigureChargePoint Request Body: <br /> {graphQl.Query}</div>");

            var result = GraphQLHelper.GraphQLRequest(graphQl, token);
            telemetryClient.TrackTrace($"GraphQL ConfigureChargePoint Response: {result}");


            var resultData = JsonConvert.DeserializeObject<dynamic>(result);

            if (resultData.data != null && Convert.ToString(resultData.data) != "{}")
            {
                logger.StatusMessage(1, $"<div><br /> GraphQL ConfigureChargePoint Response: </div><div> {resultData.data}</div>");
                logger.StatusMessage(1, "<div> ConfigureChargePoint request executed successfully.</div>");
            }
            else
            {
                if (resultData.errors != null)
                {
                    logger.StatusMessage(1, $"<br /><div>GraphQL ConfigureChargePoint Error:</div>");
                    new GraphQLHelper(logger, telemetryClient).GraphQLErrorLog(Convert.ToString(resultData));
                }

                logger.StatusMessage(1, "<div> ConfigureChargePoint request fail.</div>");
            }
        }

        private void ControllerV1AccessControl(Entity charger, string accessControl)
        {
            try
            {
                string apiURL = Convert.ToString(charger.Attributes["gk_endpoint"]).Replace("$1", string.Empty);
                string identifier = charger.Attributes.Contains("gk_chargepointname") ? Convert.ToString(charger.Attributes["gk_chargepointname"]) : string.Empty;

                using (var controllerClient = new HttpClient())
                {
                    controllerClient.BaseAddress = new Uri(apiURL);
                    ConfigureChargePointControllerV1RequestMessage configureChargePointControllerV1RequestMessage = new ConfigureChargePointControllerV1RequestMessage();
                    configureChargePointControllerV1RequestMessage.messageType = "setAccessControlRequest";
                    configureChargePointControllerV1RequestMessage.transactionId = Guid.NewGuid();
                    configureChargePointControllerV1RequestMessage.identifier = identifier;
                    configureChargePointControllerV1RequestMessage.accessControl = GetEnumAccessControlTypeName(Convert.ToInt32(accessControl));
                   
                    HttpResponseMessage res = controllerClient.PostAsJsonAsync($"AccessControl", configureChargePointControllerV1RequestMessage).Result;

                    logger.StatusMessage(1, $"<div><br/></div><div>ControllerV1AccessControl Request API : <div><br/></div> {res.RequestMessage.RequestUri.OriginalString} </div>");
                    logger.StatusMessage(1, $"<div><br/></div><div>Payload : <div><br/></div> {JsonConvert.SerializeObject(configureChargePointControllerV1RequestMessage)} </div>");

                    telemetryClient.TrackTrace($"ControllerV1AccessControl Request  : {JsonConvert.SerializeObject(configureChargePointControllerV1RequestMessage)}");

                    var responseString = res.Content.ReadAsStringAsync().Result;
                    if (res.IsSuccessStatusCode)
                    {
                        using (HttpContent content = res.Content)
                        {
                            logger.StatusMessage(1, $"<div><br/></div><div> AccessControl Response : <div><br/></div>" + responseString + "</div>");
                            telemetryClient.TrackTrace($"ControllerV1AccessControl Response: {responseString}");
                        }
                    }
                    else
                    {
                        logger.StatusMessage(1, $"<div><br/></div><div style='color:red;> AccessControl Response :  <div><br/></div> {responseString} </div>");
                        telemetryClient.TrackTrace($"AccessControl Response : {responseString}");
                    }
                }

            }
            catch (Exception ex)
            {
                logger.StatusMessage(1, $"<br/><div style='color:red;>Error Response :  <div><br/></div> {ex.Message} </div>");
                telemetryClient.TrackTrace($"Error Response :{ex.Message}");
                throw;
            }
        }

        public string GetEnumAccessControlTypeName(int accessControl)
        {
            string accessControlTypeName = "open";

            if (accessControl == (int)EnumAccessControlType.OPEN)
            {
                accessControlTypeName = "open";
            }
            else if (accessControl == (int)EnumAccessControlType.CONTROLLER_OPEN)
            {
                accessControlTypeName = "controller";
            }
            else if (accessControl == (int)EnumAccessControlType.CLOSED)
            {
                accessControlTypeName = "closed";
            }
            return accessControlTypeName;
        }

    }
}
