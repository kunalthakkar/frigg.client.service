﻿using Frigg.Client.Service.Helper;
using Frigg.Client.Service.Hubs;
using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Tooling.Connector;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Frigg.Client.Service.Controllers
{
    [RoutePrefix("api")]
    public class ControllerSetupController : ApiControllerWithHub<ControllerV2Hub>
    {
        IOrganizationService service;
        private readonly TransactionLogger logger;
        private readonly TelemetryClient telemetryClient;

        public ControllerSetupController()
        {
            logger = new TransactionLogger();
            telemetryClient = Helper.Helper.GetTelemetryClient();
            logger.OnStatusMessage += (sender, level, message) => Hub.Clients.All.message(message, level);
            logger.ProgressChanged += (sender, progress) => Hub.Clients.All.progress(progress);
        }

        [HttpPost]
        [Route("SetupController")]
        public HttpResponseMessage SetupController(ControllerSetupRequestMessage request)
        {
            try
            {
                telemetryClient.TrackTrace($"SetupController request : {JsonConvert.SerializeObject(request)}");

                var connectionString = Helper.AzureHelper.GetValueFromAzureVault("DynamicsConnectionWithAppUser");
                var service = CRMHelper.GetCRMService(connectionString + ";RequireNewInstance=true");
               
                var controllerId = ControllerSetupHelper.CreateController(service, request);

                if (controllerId != null)
                {
                    logger.StatusMessage(1, "<div>" + ControllerSetupHelper.GetLogValues(5, request.ControllerName + " created.") + "</div>");

                    var clusterList = ControllerSetupHelper.CreateClusters(service, request, controllerId, logger);

                    var response = new ControllerSetupResponseMessage
                    {
                        IsControllerCreated = true,
                        ControllerIndex = request.ControllerIndex
                    };

                    if (clusterList != null && clusterList.Count > 0)
                    {
                        ControllerSetupHelper.SetParentClusters(service, clusterList);
                    }

                    ControllerSetupHelper.AttachControllerInAcProject(service, request, controllerId);

                    logger.StatusMessage(1, request.ControllerName + " completed.");

                    ControllerSetupHelper.CreateChargers(service, request, logger);

                    logger.StatusMessage(1, "<div> Setup is completed. </div>"); 

                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    telemetryClient.TrackTrace($"Controller {request.ControllerName} is not created as error has been occurred.");
                    logger.StatusMessage(1, "<div class='error'>" + ControllerSetupHelper.GetLogValues(5, request.ControllerName + " is not created as error has been occurred.") + "</div>");
                }

                return Request.CreateResponse(HttpStatusCode.Forbidden, new ControllerSetupResponseMessage { IsControllerCreated = false, ControllerIndex = request.ControllerIndex });
            }
            catch (Exception ex)
            {                
                if (!string.IsNullOrEmpty(ex.Message))
                    logger.StatusMessage(1, ex.Message);

                if (!string.IsNullOrEmpty(ex.StackTrace))
                    logger.StatusMessage(1, ex.StackTrace);

                telemetryClient.TrackException(ex, new Dictionary<string, string>
                {
                    {"APIName",  "SetupController" },
                    {"Message", ex.Message },
                    {"StackTrace ", ex.StackTrace}
                });
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
