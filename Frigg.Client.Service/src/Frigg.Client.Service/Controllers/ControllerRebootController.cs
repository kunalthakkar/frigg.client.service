﻿using Frigg.Client.Service.Helper;
using Frigg.Client.Service.Hubs;
using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Frigg.Client.Service.Controllers
{
    [RoutePrefix("api")]
    public class ControllerRebootController : ApiControllerWithHub<ControllerConfigControllerHub>
    {
        private readonly TransactionLogger logger;
        private readonly TelemetryClient telemetryClient;


        public ControllerRebootController()
        {
            logger = new TransactionLogger();
            logger.OnStatusMessage += (sender, level, message) => Hub.Clients.All.message(message, level);
            logger.ProgressChanged += (sender, progress) => Hub.Clients.All.progress(progress);
            telemetryClient = Helper.Helper.GetTelemetryClient();
        }

        [HttpPost]
        [Route("RebootController")]
        public HttpResponseMessage RebootController(ControllerRebootRequestMessage request)
        {
            try
            {
                telemetryClient.TrackTrace($"RebootController API Request  {JsonConvert.SerializeObject(request)}");
                logger.StatusMessage(1, $"<div>Reboot Controller Request execution started.</div>");
                var connectionString = Helper.AzureHelper.GetValueFromAzureVault("DynamicsConnectionWithAppUser");
                var service = CRMHelper.GetCRMService(connectionString + ";RequireNewInstance=true");
                Guid controllerId = new Guid(request.ControllerId);
                Entity controller = service.Retrieve("gk_controller", controllerId, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
                if (controller != null)
                {
                    if (controller.Attributes.Contains("gk_serialnumber"))
                    {
                        string serialNumber = Convert.ToString(controller.Attributes["gk_serialnumber"]);
                        logger.StatusMessage(1, $"<div>Serial Number : {serialNumber}</div>");
                        telemetryClient.TrackTrace($"Serial Number  {serialNumber}");
                        string token = new GraphQLHelper(logger, telemetryClient).CreateControllerV2NewToken();
                        CallGraphQL(serialNumber, token);
                    }
                    else
                    {
                        logger.StatusMessage(1, "<div> Serial Number not found.</div>");
                        telemetryClient.TrackTrace("Serial Number not found.");
                    }
                }
                else
                {
                    logger.StatusMessage(1, "<div> Controller not found.</div>");
                    telemetryClient.TrackTrace("Controller not found.");
                }
                logger.StatusMessage(1, "<div><br /></div><div>Reboot Controller Request execution completed.</div>");
                telemetryClient.TrackTrace("Reboot Controller Request execution completed.");
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    logger.StatusMessage(1, ex.Message);
                    telemetryClient.TrackException(ex);
                }

                if (!string.IsNullOrEmpty(ex.StackTrace))
                    logger.StatusMessage(1, ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }

        private void CallGraphQL(string serialNumber, string token)
        {
            GraphQlQuery graphQl = new GraphQlQuery();
            var query = $@"mutation{{
                            rebootController(
                            deviceId: ""{serialNumber}""
                            )
                         }}";

            graphQl.Query = query;

            telemetryClient.TrackTrace($"GraphQL ControllerReboot Request Body: {graphQl.Query}");

            var result = GraphQLHelper.GraphQLRequest(graphQl, token);

            var resultData = JsonConvert.DeserializeObject<dynamic>(result);

            if (resultData.data != null && Convert.ToString(resultData.data) != "{}")
            {
                logger.StatusMessage(1, "<div style='margin-top:10px;'> GraphQL ControllerReboot Response:</div>");
                logger.StatusMessage(1, "<div>" + Convert.ToString(resultData.data) + "</div>");
                telemetryClient.TrackTrace($"GraphQL ControllerReboot Response: {Convert.ToString(resultData.data)}");
            }
            else
            {
                if (resultData.errors != null)
                {
                    telemetryClient.TrackTrace($"GraphQL ControllerReboot Error {Convert.ToString(resultData.errors)}");
                    logger.StatusMessage(1, "<div style='margin-top:10px;'>GraphQL ControllerReboot Error:</div>");
                    new GraphQLHelper(logger, telemetryClient).GraphQLErrorLog(Convert.ToString(resultData));
                }

                logger.StatusMessage(1, "<div> Controller Reboot fail.</div>");
            }
        }
    }
}
