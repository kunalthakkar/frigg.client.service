﻿using Frigg.Client.Service.Helper;
using Frigg.Client.Service.Hubs;
using Frigg.Client.Service.Models;
using GK.Helper;
using Microsoft.ApplicationInsights;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Frigg.Client.Service.Controllers
{
    [RoutePrefix("api")]
    public class ControllerConfigController : ApiControllerWithHub<ControllerConfigControllerHub>
    {
        private readonly TransactionLogger logger;
        private readonly TelemetryClient telemetryClient;
        ControllerConfigHelper controllerConfigHelper = null;


        public ControllerConfigController()
        {
            logger = new TransactionLogger();
            logger.OnStatusMessage += (sender, level, message) => Hub.Clients.All.message(message, level);
            logger.ProgressChanged += (sender, progress) => Hub.Clients.All.progress(progress);
            telemetryClient = Helper.Helper.GetTelemetryClient();
            controllerConfigHelper = new ControllerConfigHelper(logger, telemetryClient);
        }

        [HttpPost]
        [Route("ControllerConfig")]
        public HttpResponseMessage ControllerConfig(ControllerConfigRequestMessage request)
        {
            try
            {
                telemetryClient.TrackTrace($"ControllerConfig API Request  {JsonConvert.SerializeObject(request)}");

                controllerConfigHelper.ControllerConfigRequest(request);

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    logger.StatusMessage(1, ex.Message);
                    telemetryClient.TrackException(ex);
                }

                if (!string.IsNullOrEmpty(ex.StackTrace))
                    logger.StatusMessage(1, ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
