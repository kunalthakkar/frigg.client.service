﻿using Microsoft.AspNet.SignalR;

namespace Frigg.Client.Service.Hubs
{
    public class ControllerV2Hub : Hub
    {
    }
    public class ControllerConfigControllerHub : Hub
    {
    }
    public class KlarnaPaymentHub : Hub
    {
    }
    public class ChargerUserHub : Hub
    {
    }
    public class ChargingFacilityControllerHub : Hub
    {
    }
    public class AccessControlHub : Hub
    {
    }
}