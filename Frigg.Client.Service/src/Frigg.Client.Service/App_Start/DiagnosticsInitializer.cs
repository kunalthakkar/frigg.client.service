﻿using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.Extensibility;
using System.Configuration;

namespace Frigg.Client.Service.App_Start
{
    public class DiagnosticsInitializer : ITelemetryInitializer
    {
        public void Initialize(ITelemetry telemetry)
        {
            telemetry.Context.Properties["Enviroment"] = ConfigurationManager.AppSettings["DiagnosticsEnviroment"];
            telemetry.Context.Properties["ApplicationName"] = ConfigurationManager.AppSettings["DiagnosticsAppName"];
        }
    }
}