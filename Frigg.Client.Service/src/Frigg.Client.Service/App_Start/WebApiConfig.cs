﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Routing;
using Thinktecture.IdentityModel.WebApi.Authentication.Handler;
using Frigg.Client.Service.Filters;
using Frigg.Client.Service.App_Start;

namespace Frigg.Client.Service
{
    public static class WebApiConfig
    {
        private static string configUsername;
        private static string configPassword;
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes(new CustomDirectRouteProvider());

            var instrumentationKey = ConfigurationManager.AppSettings["APPINSIGHTS_INSTRUMENTATIONKEY"];
            if (!string.IsNullOrEmpty(instrumentationKey))
            {
                Microsoft.ApplicationInsights.Extensibility.TelemetryConfiguration.Active.InstrumentationKey = instrumentationKey;
            }
            else
            {
                Microsoft.ApplicationInsights.Extensibility.TelemetryConfiguration.Active.DisableTelemetry = true;
            }

            var allowedOrigins = ConfigurationManager.AppSettings["AllowedOrigin"];
            var cors = new EnableCorsAttribute(allowedOrigins, "*", "*");
            config.EnableCors(cors);

            config.Filters.Add(new ValidateModel());

            configUsername = ConfigurationManager.AppSettings["APIUsername"];
            configPassword = ConfigurationManager.AppSettings["Password"];

            var authConfig = new AuthenticationConfiguration
            {
                RequireSsl = bool.Parse(ConfigurationManager.AppSettings["RequireSSL"])
            };

            config.SuppressDefaultHostAuthentication();
            authConfig.AddBasicAuthentication(validateUser);
            config.MessageHandlers.Add(new AuthenticationHandler(authConfig));
            config.Filters.Add(new AuthorizeAttribute());

            // Web API routes


            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Services.Add(typeof(IExceptionLogger), new ApiExceptionLogger());
        }
        public class CustomDirectRouteProvider : DefaultDirectRouteProvider
        {
            protected override IReadOnlyList<IDirectRouteFactory> GetActionRouteFactories(HttpActionDescriptor actionDescriptor)
            {
                return actionDescriptor.GetCustomAttributes<IDirectRouteFactory>(inherit: true);
            }
        }
        private static bool validateUser(string userName, string password)
        {
            if (userName == configUsername && password == configPassword)
            {
                return true;
            }

            return false;
        }
    }
}
