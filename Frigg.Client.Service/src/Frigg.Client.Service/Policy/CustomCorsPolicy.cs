﻿using Microsoft.Owin.Cors;
using System.Configuration;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Cors;

namespace Frigg.Client.Service.Policy
{
    public class CustomCorsPolicy : ICorsPolicyProvider, System.Web.Http.Cors.ICorsPolicyProvider
    {
        private readonly CorsPolicy policy;

        public CustomCorsPolicy()
        {
            policy = new CorsPolicy();
            policy.AllowAnyHeader = true;
            policy.AllowAnyMethod = true;
            policy.SupportsCredentials = true;

            if (ConfigurationManager.AppSettings["AllowedOrigin"] == "*")
            {
                policy.AllowAnyOrigin = true;
            }
            else
            {
                policy.Origins.Add(ConfigurationManager.AppSettings["AllowedOrigin"]);
            }
        }
        public Task<CorsPolicy> GetCorsPolicyAsync(Microsoft.Owin.IOwinRequest request)
        {
            return Task.FromResult(policy);
        }

        public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(policy);
        }
    }
}