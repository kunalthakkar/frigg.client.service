﻿namespace Frigg.Client.Service.Diagnostics
{
    public interface ILogger
    {
        void ReportProgress(int progress);

        void StatusMessage(int level, string text);
    }
}
