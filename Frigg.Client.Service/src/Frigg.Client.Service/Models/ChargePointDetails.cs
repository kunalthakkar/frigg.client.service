﻿using System;
namespace Frigg.Client.Service.Models
{
    public class ChargePointDetails
    {
        public string ChargerName { get; set; }
        
        public string IP { get; set; }

        public int MaxAmpere { get; set; }

        public Guid ChargerId { get; set; }
    }
}