﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Models
{
    public class ChargingFacilityResponse
    {
        [JsonProperty("evseId")]
        public string EvseId { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }
        [JsonProperty("transactionId")]
        public string TransactionId { get; set; }

    }
}