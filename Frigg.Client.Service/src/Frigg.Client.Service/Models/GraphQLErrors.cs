﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Models
{
    public class GraphQLErrors
    {
        public List<Errors> Errors { get; set; }
    }

    public class Errors
    {
        public string Message { get; set; }
        public Extensions Extensions { get; set; }
    }

    public class Extensions 
    {
        public string Code { get; set; }
        public string errorCode { get; set; }
    }
}