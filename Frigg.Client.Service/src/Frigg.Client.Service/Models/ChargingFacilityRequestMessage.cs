﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Models
{
    public class ChargingFacilityRequestMessage
    {
        [Required]
        public string EvseId { get; set; }
        [Required]
        public string TagId { get; set; }
    }
}