﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Frigg.Client.Service.Models
{
    public class ControllerSetupRequestMessage 
    {
        public string AcProjectId { get; set; }
        public string ControllerName { get; set; }
        public decimal? FuseSize { get; set; }
        public string SerialNumber { get; set; }
        [Required]
        public string DynamicsCustomerId { get; set; }
        [Required]
        public string DynamicsLocationId { get; set; }
        public int ControllerIndex { get; set; }       
        public int NumberOfChargers { get; set; }
        public string DynamicsProductId { get; set; }
        public int? Headroom { get; set; }
        public int? SensorType { get; set; }
        public int? CurrentSensorType { get; set; }
        public decimal? Ampere { get; set; }
        public int NetType { get; set; }
        public List<ClusterDetails> ClusterDetails { get; set; }
    }

    public class ClusterDetails
    {
        public string Name { get; set; }
        public int? Limit { get; set; }
        public int? Headroom { get; set; }
        public int? Priority { get; set; }
        public string ParentCluster { get; set; }
    }

    public class Clusters
    {
        public Guid ClusterId { get; set; }
        public string Name { get; set; }
        public string ParentCluster { get; set; }
    }

    public class ControllerSetupResponseMessage
    {
        public bool IsControllerCreated { get; set; }
        public int ControllerIndex { get; set; }
    }

    public class ConnectorDetails
    {
        public Guid? ConnectorType { get; set; }
        public int? ConnectorLevel { get; set; }
        public int? StartChargringScenario { get; set; }
    }
}