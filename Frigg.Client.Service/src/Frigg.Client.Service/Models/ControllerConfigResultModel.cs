﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Models
{
    public class ControllerConfigResultModel
    {

    }

    public class ScanControllerResultModel
    {
        [JsonProperty("scan")]
        public List<Scan> Scan { get; set; }
    }

    public class Scan
    {
        public string Id { get; set; }
        public string IP { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string Vendor { get; set; }
    }

    public class InitiateChargePointResultModel
    {
        public bool InitiateChargepoints { get; set; }

    }

    public class SetSensorAnalogResultModel
    {
        public ChargePoint ChargePoint { get; set; }
        public List<ChargePoint> ChargePoints { get; set; }
        public Cluster Cluster { get; set; }
        public List<Cluster> Clusters { get; set; }

        public ControllerConfig Configuration { get; set; }
        public string Id { get; set; }
        public Sensor Sensor { get; set; }
        public List<Sensor> Sensors { get; set; }
        public string Setup { get; set; }
        public SyestemState State { get; set; }
    }

    public class SetSensorModBusResultModel
    {
        public ChargePoint ChargePoint { get; set; }
        public List<ChargePoint> ChargePoints { get; set; }
        public Cluster Cluster { get; set; }
        public List<Cluster> Clusters { get; set; }

        public ControllerConfig Configuration { get; set; }
        public string Id { get; set; }
        public Sensor Sensor { get; set; }
        public List<Sensor> Sensors { get; set; }
        public string Setup { get; set; }
        public SyestemState State { get; set; }
    }

    public class ConfigureControllerResultModel
    {
        public ChargePoint ChargePoint { get; set; }
        public List<ChargePoint> ChargePoints { get; set; }
        public Cluster Cluster { get; set; }
        public List<Cluster> Clusters { get; set; }

        public ControllerConfig Configuration { get; set; }
        public string Id { get; set; }
        public Sensor Sensor { get; set; }
        public List<Sensor> Sensors { get; set; }
        public string Setup { get; set; }
        public SyestemState State { get; set; }
    }

    public class SyestemState 
    {
        public float CPULoad { get; set; }
        public int FreeDiskSpace { get; set; }
    }

    public class Sensor
    {
        public SensorConfig configuration { get; set; }
        public string Id { get; set; }
    }

    public class ControllerConfig
    {
        public ModBusMasterConfig ModBusMasterConfiguration { get; set; }
        public RegulatorConfig RegulatorConfiguration { get; set; }
        public SensorConfig SensorReaderConfiguration { get; set; }

    }

    public class SensorConfig
    {
        public ModBusConfig ModBusConfig { get; set; }
        public AnalogSensorConfig AnalogSensorConfig { get; set; }

    }
    public class ModBusConfig
    {
        public string Model { get; set; }
        public string Name { get; set; }
        public string SlaveId { get; set; }
    }

    public class AnalogSensorConfig
    {
        public string Filter { get; set; }
        public float InputCurrent { get; set; }
        public string Name { get; set; }
        public float OutputVoltage { get; set; }
        public EnumCurrentSensorType SensorType { get; set; }
    }

    public class RegulatorConfig
    {
        public bool DisabledLoadBalancing { get; set; }
    }

    public class ModBusMasterConfig
    {
        public int BaudRate { get; set; }
        public int DataBits { get; set; }
        public int Parity { get; set; }
        public int PollIntervalMs { get; set; }
        public int StopBits { get; set; }
    }

    public class ChargePoint
    {
        public Cluster Cluster { get; set; }

        public ChargePointConfig Configuration { get; set; }
        public string Id { get; set; }
        public ChargePointState state { get; set; }
    }

    public class ChargePointState
    {
        public ConnectorState Connector { get; set; }
        public List<ConnectorState> Connectors { get; set; }
        public string Id { get; set; }
        public string Ip { get; set; }
    }

    public class ConnectorState
    {
        public string Id { get; set; }
        public EnumConnectorStateType State { get; set; }
    }

 

    public class ChargePointConfig
    {
        public EnumAccessControlType AccessControlType { get; set; }
        public string ClusterId { get; set; }
        public ConnectorConfig Connectors { get; set; }

    }

    public class ConnectorConfig
    {
        public string Id { get; set; }
        public float MinAmpere { get; set; }
        public float MaxAmpere { get; set; }
        public string PhaseMapping { get; set; }
        public EnumChargePriority Priority { get; set; }
    }

   

    public class Cluster
    {
        public ClusterConfig Configuration { get; set; }
        public string Id { get; set; }
    }

    public class ClusterConfig
    {
        public float FuseLimit { get; set; }
        public EnumNetType NetType { get; set; }
        public string Parent { get; set; }
    }

  
}