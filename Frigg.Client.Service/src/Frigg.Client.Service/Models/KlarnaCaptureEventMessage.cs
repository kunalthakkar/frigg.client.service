﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Frigg.Client.Service.Models
{
    public class KlarnaCapureEventRequestMessage
    {
        [Required]
        public Guid OrderId { get; set; }
    }

    public class KlarnaCaptureEventResponseMessage
    {
        public bool Success { get; set; }
    }

    public class OrderDetails
    {
        public Guid OrderId { get; set; }

        public string OrderNumber { get; set; }

        public double AmountIncVat { get; set; } 

        public string Currency { get; set; }

        public string PspReference { get; set; }
    }
}