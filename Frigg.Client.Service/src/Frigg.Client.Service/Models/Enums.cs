﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Models
{
    public class Enums
    {
    }

    public enum EnumVendor
    {
        SCHNEIDER = 821200000,
        VESTEL = 821200001,
        IO_CHARGE = 821200002
    }

    public enum EnumCurrentSensorType
    {
        CT = 821200000,
        ROGOWSKI = 821200001
    }

    public enum EnumConnectorStateType
    {
        AVAILABLE,
        PREPARING,
        CHARGING,
        SUSPENDED,
        FAULTED,
        DISCONNECTED
    }

    public enum EnumChargePriority
    {
        LOW,
        NORMAL,
        HIGH
    }

    public enum EnumNetType
    {
        IT = 821200000,
        TT = 821200001,
        TN = 821200002
    }

    public enum EnumAccessControlType
    {
        OPEN = 821200000,
        CLOSED = 821200002,
        CONTROLLER_OPEN = 821200001,
        CONTROLLER_CLOSED = 821200003
    }

    public enum EnumModel
    {
        CARLO_GAVAZZI_EM210,
        SCHNEIDER_IEM3555
    }

    public enum EnumControllerSensorType
    {
        Analog = 821200000,
        Modbus = 821200001,
        Fake = 821200002,
        Playback = 821200003
    }

}