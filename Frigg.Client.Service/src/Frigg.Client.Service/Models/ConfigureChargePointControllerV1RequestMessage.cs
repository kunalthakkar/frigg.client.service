﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Models
{
    public class ConfigureChargePointControllerV1RequestMessage
    {
        public string messageType { get; set; }
        public Guid transactionId { get; set; }
        public string timestamp { get; set; } = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
        public string identifier { get; set; }
        public string accessControl { get; set; }
    }
}