﻿using Frigg.Client.Service.Diagnostics;

namespace Frigg.Client.Service.Models
{
    public class TransactionLogger : ILogger
    {
        public event ProgressChanged ProgressChanged;
        public event OnStatusMessage OnStatusMessage;
        public int TransferLogId { get; }
        public string TransferLogIdString { get; }
        public void SetProgressMin(int min)
        {

        }

        public void SetProgressMax(int max)
        {

        }

        public int CurrentStepProgressMin { get; set; }
        public int CurrentStepProgressMax { get; set; }

        public void ReportProgress(int progress)
        {
            ProgressChanged?.Invoke(this, progress);
        }

        public void StatusMessage(int level, string text)
        {
            OnStatusMessage?.Invoke(this, level, text);
        }
    }

    public delegate void ProgressChanged(object sender, int progress);
    public delegate void OnStatusMessage(object sender, int level, string message);
}