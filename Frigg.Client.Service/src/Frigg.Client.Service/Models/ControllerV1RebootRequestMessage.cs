﻿using System;
using System.Collections.Generic;

namespace Frigg.Client.Service.Models
{
    public class ControllerV1RebootRequestMessage
    {
        public string messageType { get; set; }
        public Guid transactionId { get; set; }
        public string timestamp { get; set; } = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
        public string identifier { get; set; }

        public bool rebootAll { get; set; }

    }
}