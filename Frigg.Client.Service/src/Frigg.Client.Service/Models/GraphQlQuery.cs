﻿using Newtonsoft.Json;
namespace Frigg.Client.Service.Models
{
    public class GraphQlQuery
    {
        [JsonProperty("query")]
        public string Query { get; set; }
    }
}