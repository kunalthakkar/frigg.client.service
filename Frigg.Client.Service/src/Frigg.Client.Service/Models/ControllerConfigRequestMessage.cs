﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Models
{
    public class ControllerConfigRequestMessage
    {
        public string ControllerId { get; set; }
        public string VendorId { get; set; }
        public ControllerConfigRequestEnum ControllerConfigRequestType { get; set; }
        public string ChargerId { get; set; } = string.Empty;
        public bool IsSelectAll { get; set; } = true;
    }

    public enum ControllerConfigRequestEnum
    {
        ScanController,
        InitiateChargepointController,
        ConfigureController,
        SetSensorAnalogController,
        SetSensorModbusController,
        AllEvents
    }
}