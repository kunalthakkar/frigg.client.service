﻿namespace Frigg.Client.Service.Models
{
    public class ControllerRebootRequestMessage
    {
        public string ControllerId { get; set; }
    }

    public class ControllerRebootChargerRequestMessage
    {
        public string ControllerId { get; set; }
        public bool IsSelectAll { get; set; }
        public string ChargerId { get; set; }
    }
}