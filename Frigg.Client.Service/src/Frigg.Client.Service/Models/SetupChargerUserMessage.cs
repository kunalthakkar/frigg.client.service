﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Frigg.Client.Service.Models
{
    public class SetupChargerUserRequestMessage
    {
        public List<ChargerUserDetails> ChargerUsers { get; set; }

        public Guid DynamicsChargerUserGroupId { get; set; }

        public string ChargerUserGroupName { get; set; }

        [JsonIgnore]
        public Guid DynamicsChargerUserId { get; set; }
    }

    public class ChargerUserDetails
    {
        public Guid DynamicsAccountId { get; set; }

        public string Uid { get; set; }
    }
}