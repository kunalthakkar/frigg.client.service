﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frigg.Client.Service.Models
{
    public class ConfigureChargePointRequestMessage
    {
        public string ChargerId { get; set; }
        public string DeviceId { get; set; }
        public string AccessControl { get; set; }

        public string ActualChargerId { get; set; }
    }
}