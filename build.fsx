#r @"tools/FAKE/tools/FakeLib.dll"
open System
open Fake
open Fake.AssemblyInfoFile
open Fake.Testing.NUnit3

let buildNumber = getBuildParamOrDefault "buildNumber" "0"
let azureServicePrincipal = getBuildParamOrDefault "spi" ""
let azurePassword = getBuildParamOrDefault "spiPassword" ""
let azureTenant = getBuildParamOrDefault "azureTenant" ""

let artifactsDir = "./artifacts/"
let publishDir = "./publish"
let packageDir = "./packages"
let nugetPath = "./tools/nuget.exe"
let octoCliPath = "./tools/OctopusTools/tools/Octo.exe"

let version = "1.0.0." + buildNumber
let company = "Avant IT AS"
let projectName = "Frigg.Client.Service"
let description = ""
let solutionFile = "./Frigg.Client.Service/Frigg.Client.Service.sln"

let rootPath = (directoryInfo ".").FullName
let publishPath = (directoryInfo publishDir).FullName
let artifactsPath = (directoryInfo artifactsDir).FullName

let azCommand = tryFindFileOnPath "az.cmd"

let projects = [
    ("Frigg.Client.Service", "\\Frigg.Client.Service\\src\\Frigg.Client.Service\\Frigg.Client.Service.csproj");
    ]

let testProjects = [
     ("Frigg.Client.Service.Test", "\\Frigg.Client.Service\\test\\Frigg.Client.Service.Test\\Frigg.Client.Service.Test.csproj");
     ]

let azLogout =
    let args = sprintf "logout"
    let result = match azCommand with
                      | Some c -> Shell.Exec(c, args)
                      | None -> failwith "Cannot find Azure CLI"
    
    result


Target "Login azure" (fun _ ->
    azLogout |> ignore
    
    let args = sprintf "login --service-principal -u %s -p %s --tenant %s" azureServicePrincipal azurePassword azureTenant
    
    let result = match azCommand with
                      | Some c -> Shell.Exec(c, args)
                      | None -> failwith "Cannot find Azure CLI"
    
    if result <> 0 then failwithf "Cannot login to azure with azure CLI"
)

Target "Logoff azure" (fun _ ->
    azLogout |> ignore
)

Target "Clean" (fun _ ->
    SetBuildNumber version

    CleanDirs [artifactsDir;publishDir]
)


Target "Install Octo" (fun _ ->
    let result = ExecProcess (fun info ->
        info.FileName <- nugetPath
        info.WorkingDirectory <- "./"
        info.Arguments <- "Install OctopusTools -Version 4.30.7 -OutputDirectory tools -ExcludeVersion") (TimeSpan.FromMinutes 5.0)
        
    if result <> 0 then failwithf "nuget.exe returned with a non-zero exit code"
)

Target "Build" (fun _ ->
    CreateCSharpAssemblyInfo "./src/Frigg.Client.Service/Properties/AssemblyInfo.cs"
        [Attribute.Title projectName
         Attribute.Description description
         Attribute.Guid "c011a968-fa85-42aa-be34-ed851ee786d1"
         Attribute.Product projectName
         Attribute.Version version
         Attribute.FileVersion version
         Attribute.Company company
         Attribute.Copyright "Copyright �  2018"]

    let props = [
        "RunOctoPack", "true"
        "OctoPackNuGetProperties", "version=" + version + ";description=" + description
    ]

    MSBuildReleaseExt publishDir props "Build" ["./Frigg.Client.Service/Frigg.Client.Service.sln"]
      |> Log "Build-Output: "
)


Target "IntegrationTest" (fun _ ->  
    testProjects
    |> Seq.iter (fun (name, path) ->
        DotNetCli.Test(fun p ->
        { p with
            Project = (rootPath @@ path)
            TimeOut = TimeSpan.FromMinutes(2.0)
            AdditionalArgs = ["/p:Version=" + version]
            Configuration = "Release"
        })
        sendTeamCityMSTestImport (rootPath @@ "TestResults")
    )
)


Target "Package" (fun _ ->
    projects
    |> Seq.iter (fun (name, path) ->
        DotNetCli.Publish(fun p ->
        { p with
            Configuration = "Release"
            Output = publishPath
            Project = (rootPath @@ path)
            AdditionalArgs = ["--no-build";"/p:Version=" + version]
        })
        let singleProjectPublishDir = (publishDir @@ name)
        let result = ExecProcess(fun info ->
            info.FileName <- octoCliPath
            info.WorkingDirectory <- "./"
            info.Arguments <- "pack --id " + name + " --version=" + version + " --basePath="+  singleProjectPublishDir + " --outFolder=" + artifactsDir + " --author=\"" + company + "\" --description=\"" + description + "\"") (TimeSpan.FromMinutes 5.0)
          
        if result <> 0 then printfn "%A" result
    )
)

Target "Default" (fun _ ->
    DoNothing()
)


// Dependencies
"Clean"
  =?> ("Login azure", hasBuildParam "spiPassword")
  ==> "Install Octo"
  ==> "Build"
  =?> ("IntegrationTest", hasBuildParam "spiPassword")
//   ==> "Package"
  =?> ("Logoff azure", hasBuildParam "spiPassword")
  ==> "Default"


RunTargetOrDefault "Default"