@echo off
cls

SET BUILDNUMBER=%1%
SET AZUREUSER=%2%
SET AZUREPASSWORD=%3%
SET AZURETENANT=%4%

if not exist tools\nuget.exe (
	echo Downloading 'NuGet'
	mkdir tools
    PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& '.\download-nuget.ps1'"
)

"tools\nuget.exe" "Install" "FAKE" -Version 4.63.2 -OutputDirectory "tools" -ExcludeVersion
"tools\FAKE\tools\Fake.exe" build.fsx buildNumber=%BUILDNUMBER% spi=%AZUREUSER% spiPassword=%AZUREPASSWORD% azureTenant=%AZURETENANT%